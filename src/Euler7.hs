import Data.Functor

primes = 2:filter f [3,5..]
    where f n = all (\p -> n `mod` p /= 0) $ takeWhile (\p -> p * p <= n) primes

main :: IO ()
main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter []    = return ()
          iter (h:t) = (putStrLn $ show $ primes !! (h - 1)) >> iter t
