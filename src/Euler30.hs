import Data.Functor
import Data.Array.Unboxed (UArray)
import qualified Data.Array.Unboxed as Arr

-- derivation:
-- given any order n, 9 ^ n is the largest number,
-- so if we are supposed to use brute force, up to
-- which number we need to count to?
--
-- say the largest possible number has m digit,
-- then limit is m * 9 ^ n
--
-- let m be the minimum of these numbers, so we have
--
-- m * 9 ^ n <= 10 ^ (m + 1) - 1
--
-- since the rhs grows exponentially, once this holds, 
-- the minimum of m is found.
--
-- for m * 9 ^ n
--   = m * (10 - 1) ^ n
--   ~ m * (10 ^ n - n * 10 ^ (n - 1) <- this is n digit)
--   = m * 10 ^ n - m * n * 10 ^ (n - 1) <- this is max (n + 1) digit
--
-- hence m <= n + 1
--

digitPow d n
    | d < 10 = d ^ n
    | otherwise = let (u, l) = divMod d 10
                   in l ^ n + digitPow u n

bruteForce n = filter pred [10..ub]
    where m      = n + 1
          ub     = m * 9 ^ n
          pred d = d == digitPow d n

-- |precomputed numbers using bruteForce
o1LookUp :: UArray Int Int
o1LookUp = Arr.array (3, 6) [(3, 1301), (4, 19316), (5, 443839), (6, 548834)]

main = putStrLn =<< show <$> (o1LookUp Arr.!) <$> read <$> getLine
