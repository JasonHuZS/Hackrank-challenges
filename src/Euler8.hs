import Data.Functor
import Data.List (genericSplitAt, tails)

consecProd ns n k = map (foldl1 (*) . take k) $ take (n - k + 1) $ tails ns

main :: IO ()
main = getLine >> (iter =<< lines <$> getContents)
    where iter [] = return ()
          iter (l1:l2:t) = let [n, k] = map read $ words l1
                            in (putStrLn $ show $ maximum $ consecProd (map (\v -> read [v]) l2) n k)
                            >> iter t

