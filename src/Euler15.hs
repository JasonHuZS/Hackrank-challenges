import Data.Functor

comb n c = (foldl1 (*) [n-c+1..n]) `div` (foldl1 (*) [1..c])

main :: IO ()
main = getLine >> (iter =<< lines <$> getContents)
    where iter [] = return ()
          iter (h:t) = let [n1, n2] = map read $ words $ h
                        in (putStrLn $ show $ (comb (n1+n2) n2) `mod` 1000000007) >> iter t
