{-# LANGUAGE BangPatterns #-}
import Data.Functor
import Data.Bifunctor
import Data.Array (Array)
import qualified Data.Array as Arr
import qualified Data.Char as Char

isPanlindrome ('0':_) = False -- if it starts with 0 then it can't be
isPanlindrome s       = s == reverse s

digits = Arr.array (0, 9) $ zip [0..9] $ map (head . show) [0..9]

baseK k 0 = ['0']
baseK k n = go n []
    where go 0 arr = arr
          go n arr = let (!d, !r) = divMod n k
                     in go d $ digits Arr.! r:arr

doubPanlTups k n = filter (uncurry (&&) . bimap isPanlindrome (isPanlindrome . show)) 
                 $ map (\v -> (baseK k v, v)) [1..n-1]

doubPanlin k n = map snd $ doubPanlTups k n

main = do
    [n, k] <- map read <$> words <$> getLine
    putStrLn $ show $ sum $ doubPanlin k n
