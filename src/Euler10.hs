import Data.Functor
import qualified Data.Map as Map

primes = 2:filter f [3,5..]
    where f n = all (\p -> n `mod` p /= 0) $ takeWhile (\p -> p * p <= n) primes

sumMap = Map.fromAscList sums
    where sums = (2, 2):zipWith (\(_, s) p -> (p, s + p)) sums (tail $ takeWhile (<1000000) primes)

main :: IO ()
main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter []    = return ()
          iter (h:t) = (putStrLn $ show $ case Map.lookupLE h sumMap of
                            Nothing     -> -1
                            Just (_, v) -> v)
                     >> iter t
