import Data.Map
import Control.Monad


toLeaderBoard :: (Eq a, Num b) => [a] -> [(a, b)]
toLeaderBoard []      = []
toLeaderBoard (h:t)   = (h, 1) : go t h 2
  where go [] _ _     = []
        go (h:t) c r
          | h == c    = go t c r
          | otherwise = (h, r): go t h (r + 1)


toTree :: [Integer] -> Map Integer Integer
toTree = fromList . toLeaderBoard

main :: IO ()
main = getLine >> (toTree . fmap read . words <$> getLine) >>= \m ->
  getLine >> (fmap read . words <$> getLine) >>= \ns ->
  forM_ ns (\w ->
               case lookupGE w m of
                 Nothing     -> putStrLn "1"
                 Just (k, r) -> if k == w
                                then putStrLn $ show r
                                else putStrLn $ show $ r + 1)
