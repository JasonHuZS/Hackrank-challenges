import Data.Functor
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Int (Int64)

-- pregenerate a set of primes to speed up `isPrime`

primes :: [Int64]
primes = 2:filter f [3,5..]
    where f n = all (\p -> n `mod` p /= 0) $ takeWhile (\p -> p * p <= n) primes

limit = 10 ^ 6

primeSet = Set.fromList $ takeWhile (<=limit) primes

isPrime n =  Set.member n primeSet 
          || (n > limit && all (\p -> n `mod` p /= 0) (takeWhile (\p -> p * p <= n) primes))

rotate n = n:(go $ show n)
    where go [_]   = []
          go (h:t) = let rot = t ++ [h]
                         m   = read rot
                      in if m == n then [] 
                                   else m:go rot

findTillN n = go Set.empty $ takeWhile (<n) primes
    where go s []             = s
          go s (h:t)
            | Set.member h s = go s t
            | otherwise       = if all isPrime rot
                                   then go (Set.union (Set.fromList $ filter (<n) rot)
                                                       s)
                                           t
                                   else go s t
            where rot = rotate h

main = putStrLn =<< (show . sum . Set.toList . findTillN) <$> read <$> getLine
