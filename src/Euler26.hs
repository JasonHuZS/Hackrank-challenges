{-# LANGUAGE TupleSections #-}
import Data.Functor
import Data.Maybe
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

deciDiv n = go 10 n 1 Map.empty
    where go n d p m
            | r == 0         = Nothing
            | Map.member r m = Just $ p - m Map.! r
            | otherwise      = go (10*r) d (p+1) $ Map.insert r p m
            where (dv, r) = n `divMod` d

deciDivs = catMaybes $ map (\v -> (v,) <$> deciDiv v) [2..]

main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter []    = return ()
          iter (h:t) = (putStrLn $ show $ go h deciDivs 0 0) >> iter t
          go n ((i, l):t) j m
            | i >= n    = j
            | l > m     = go n t i l
            | otherwise = go n t j m
