import Data.Functor


evenFib = filter even fib
    where fib = 1:2:zipWith (+) fib (tail fib)


main :: IO ()
main = do
        getLine
        ls <- lines <$> getContents
        iter ls
    where iter []    = return ()
          iter (h:t) = do
              let n = read h
              putStrLn $ show $ sum $ takeWhile (<n) evenFib
              iter t
