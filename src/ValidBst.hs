import Data.Maybe


data Tree a = Node { left :: Tree a
                   , valu :: a
                   , righ :: Tree a }
            | Nil deriving Show

buildTree :: [Int] -> Maybe (Tree Int)
buildTree []    = Just Nil
buildTree (h:t) = let (left, rem)  = buildLeft (\_ -> True) h t
                      (righ, rem') = buildRight h (\_ -> True) rem
                  in case rem' of
                       []  -> Just $ Node left h righ
                       _:_ -> Nothing
  where buildLeft _ _ []     = (Nil, [])
        buildLeft pred ub l@(h:t)
          | pred h && h < ub = let (lt, rem)  = buildLeft pred h t
                                   (rt, rem') = buildRight h (<ub) rem
                               in (Node lt h rt, rem')
          | otherwise        = (Nil, l)
        buildRight _ _ []    = (Nil, [])
        buildRight lb pred l@(h:t)
          | lb < h && pred h = let (lt, rem)  = buildLeft (lb<) h t
                                   (rt, rem') = buildRight h pred rem
                               in (Node lt h rt, rem')
          | otherwise        = (Nil, l)


main :: IO ()
main = getLine >> (iter =<< lines <$> getContents)
  where iter [] = return ()
        iter (_:nums:t) = (case buildTree $ fmap read $ words nums of
                             Just _ -> putStrLn "YES"
                             Nothing -> putStrLn "NO") >>= \_ -> iter t
        iter (_:_) = return ()
