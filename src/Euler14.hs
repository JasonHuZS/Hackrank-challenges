{-# LANGUAGE BangPatterns #-}
import Data.Functor
import qualified Data.Set as Set
import Data.Array (Array)
import qualified Data.Array as Arr
import qualified Data.Map as Map
import Control.DeepSeq


genColtzFunc n = collatz
    where collatzArr = Arr.array (1, n) $ take n $ map (\v -> (v, collatz v)) [1..]
          collatz 1  = 1
          collatz !m 
              | even m    = go (m `div` 2)
              | otherwise = go (3 * m + 1)
              where go !l
                      | l <= n    = let !v = 2 + collatzArr Arr.! l in v
                      | otherwise = let !r = 2 + collatz l in r

findMax f ns = go ns 2 (1, 1)
    where go [] _ _ = []
          go (1:t) n acc = (1, 1):go t n acc
          go (h:t) n acc
            | n < h      = if cur > acc then go (h:t) (n+1) cur 
                                        else go (h:t) (n+1) acc
            | n == h     = let acc' = if cur > acc then cur else acc
                            in (n, snd acc'):go t (n+1) acc'
            | otherwise  = error "something is wrong"
            where cur = (f n, n)


main = getLine >> do
    ns <- map read <$> lines <$> getContents
    let m          = maximum ns
    let coltz      = genColtzFunc m
    let res        = Map.fromAscList $ findMax coltz $ Set.toAscList $ Set.fromList ns
    let iter []    = return ()
        iter (h:t) = (putStrLn $ show $ res Map.! h) >> iter t
    iter ns
