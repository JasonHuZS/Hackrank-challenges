{-# LANGUAGE OverloadedStrings #-}

import Data.Text as T
import Data.Text.IO as TIO

fromString :: String -> Text
fromString = T.pack

height :: (Num a, Integral b) => b -> a
height n = 2 ^ n

width :: (Num a, Integral b) => b -> a
width n = 2 * (height n) - 1

baseTrigle :: Integral p => p -> [Text]
baseTrigle n = loop 0
  where h    = height n
        w    = width n
        loop n
          | n < h = let num      = 2 * n + 1
                        spaces   = (w - num) `div` 2
                        paddings = T.replicate spaces "_"
                        ones     = T.replicate num "1"
                    in T.concat [paddings, ones, paddings] : loop (n + 1)
          | otherwise  = []


pyramid :: Integral p => p -> p -> [Text]
pyramid n base = iter n
  where btri   = baseTrigle base
        iter n
          | n == base = btri
          | n > base  = let tri  = iter (n - 1)
                            padw = height (n - 1) -- 2 ^ (n - 1)
                            pads = T.replicate padw "_"
                            top  = fmap (\t -> T.concat [pads, t, pads]) tri
                        in top ++ fmap (\t -> T.concat [t, "_", t]) tri


sierpinski :: Integer -> [Text]
sierpinski = pyramid 5 . (5-)

main :: IO ()
main = fmap read Prelude.getLine >>= (printLines . sierpinski)
  where printLines [] = return ()
        printLines (h:t) = TIO.putStrLn h >> printLines t
