{-# LANGUAGE BangPatterns #-}
import Data.Functor
import Data.Array (Array)
import qualified Data.Array as Arr

-- 
-- derivation
-- a typical dynamic programming problem
-- let series a[k], k >= 0 specifies different combination of coins that
-- sums up to k, then we have
--        |  1, k = 0
-- a[k] = {  1, k = 1
--        |  b[k, 200], k > 1
-- where b[k, l] is an auxiliary series, where l is the maximum value of
-- coins that are possibly used. hence:
--           |  1, l = 1 
-- b[k, l] = {  b[k - l, l] + b[k, c[index(c, l) - 1]]
--  where c = [1, 2, 5, 10, 20, 50, 100, 200]
--


genCountFunc cs ub = count
    where size     = length cs
          coins    = Arr.array (1, size) $ zip [1..] cs
          memo     = Arr.array ((1, 0), (size, ub)) [((c, m), go c m) | c <- [1..size] , m <- [0..ub]]
          count n  = memo Arr.! (size, n)
          -- | c is the index of maximum value of coins being used
          -- m is the total amount to reach
          go 0 _   = error "c reaches 0, something is wrong"
          go _ 0   = 1
          go !c !m = case cn of
                       1             -> 1
                       _ | m < cn    -> let !r = memo Arr.! (c - 1, m) in r
                         | otherwise -> let !consumed = memo Arr.! (c, m - cn)
                                            !enough   = memo Arr.! (c - 1, m)
                                         in consumed + enough
               where cn = coins Arr.! c
                    
stdCoins = [1, 2, 5, 10, 20, 50, 100, 200]

main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where countCombs = genCountFunc stdCoins 100000
          iter []    = return ()
          iter (h:t) = (putStrLn $ show $ countCombs h `mod` 1000000007) >> iter t
