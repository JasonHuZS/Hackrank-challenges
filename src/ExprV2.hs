{-# LANGUAGE FlexibleContexts #-}
-- import Text.Parsec as Par hiding (spaces)
import Text.ParserCombinators.Parsec as Par hiding (spaces)
import Control.Monad as Mon


data Expr = Number Integer
          | Unary  Bool Expr
          | Add    Expr Expr
          | Sub    Expr Expr
          | Mul    Expr Expr
          | Div    Expr Expr


instance Show Expr where
    show (Number n)  = show n
    show (Unary s n) = (if s then "" else "-") ++ show n
    show (Add l r)   = "(" ++ (show l) ++ "+" ++ (show r)  ++ ")"
    show (Sub l r)   = "(" ++ (show l) ++ "-" ++ (show r)  ++ ")"
    show (Mul l r)   = "(" ++ (show l) ++ "*" ++ (show r)  ++ ")"
    show (Div l r)   = "(" ++ (show l) ++ "/" ++ (show r)  ++ ")"


spaces :: Parser ()
spaces = skipMany space

parseNumber :: Parser Expr
parseNumber = liftM (Number . read) $ many1 digit

parseExpr :: Parser Expr
parseExpr = do
    term <- parseTerm
    spaces
    opt  <- optionMaybe $ do
        op   <- oneOf "+-"
        spaces
        expr <- parseExpr
        return (op, expr)
    return $ case opt of
        Nothing          -> term
        Just ('+', expr) -> Add term expr
        Just ('-', expr) -> Sub term expr

parseTerm :: Parser Expr
parseTerm = do
    factr <- parseFactor
    spaces
    opt   <- optionMaybe $ do
        op    <- oneOf "*/"
        spaces
        term  <- parseTerm
        return (op, term)
    return $ case opt of
        Nothing          -> factr
        Just ('*', term) -> Mul factr term
        Just ('/', term) -> Div factr term

parseUnary :: Parser Expr
parseUnary = do
    spaces
    c    <- oneOf "+-"
    expr <- parseFactor
    return $ Unary (c == '+') expr

parseFactor :: Parser Expr
parseFactor =   parseNumber 
            <|> parseUnary 
            <|> do char '(' >> spaces
                   expr <- parseExpr
                   spaces >> char ')'
                   return expr

parse' ::  String -> Either ParseError Expr
parse' = parse parseExpr ""

xEuclidean ::  Integral t => t -> t -> (t, t)
xEuclidean a b = iter a b 1 0 0 1
    where iter r0 r1 s0 s1 t0 t1 =
            if r == 0 then (s1, t1)
                    else iter r1 r s1 s t1 t
            where (q, r) = r0 `divMod` r1
                  s      = s0 - q * s1
                  t      = t0 - q * t1


eval ::  Integer -> Expr -> Integer
eval p expr = _eval expr
    where divp n = let (r, _) = xEuclidean n p in (r + p) `mod` p
          _eval (Number n)  = n `mod` p
          _eval (Unary s e) = let r = _eval e
                               in if s || r == 0 then r else p - r
          _eval (Add e1 e2) = let r1 = _eval e1
                                  r2 = _eval e2
                               in (r1 + r2) `mod` p
          _eval (Sub e1 e2) = let r1 = _eval e1
                                  r2 = _eval e2
                               in (r1 - r2 + p) `mod` p
          _eval (Mul e1 e2) = let r1 = _eval e1
                                  r2 = _eval e2
                               in (r1 * r2) `mod` p
          _eval (Div e1 e2) = let r1 = _eval e1
                                  r2 = _eval e2
                               in (r1 * (divp r2)) `mod` p

eval' ::  String -> Either ParseError Integer
eval' = liftM (eval 1000000007) . parse'

main :: IO ()
main = do
    Right v <- liftM eval' getLine
    putStrLn $ show v
