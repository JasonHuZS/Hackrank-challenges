import Data.Functor
import Data.IntSet (IntSet)
import qualified Data.IntSet as Set

primes = 2:filter f [3,5..]
    where f n = all (\p -> n `mod` p /= 0) $ takeWhile (\p -> p * p <= n) primes

decomp n = iter n primes
    where factor n p acc
            | n `mod` p /= 0 = (n, acc)
            | otherwise      = factor (n `div` p) p (1 + acc)
          iter 1 _           = []
          iter m (p:ps)
            | p * p > m      = [(m, 1)]
            | m `mod` p == 0 = let (m', o) = factor m p 0 in (p, o):iter m' ps
            | otherwise      = iter m ps

divisorSum n = go decp - n
    where decp = decomp n
          go []         = 1
          go ((p, n):t) = let dvs = go t
                           in (p ^ (n+1) - 1) `div` (p - 1) * dvs

abundants = filter (\v -> v < divisorSum v) [12..28123]
abundantSet = Set.fromList abundants

isAbunSum n 
    | n <= 28123 = any (flip Set.member abundantSet . (n-)) $ takeWhile (<=n2) abundants
    | otherwise  = True
    where n2 = n `div` 2


main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter [] = return ()
          iter (h:t) = (putStrLn $ if isAbunSum h then "YES" else "NO") >> iter t
