import Data.Array
import Data.Int

fence :: Array Int64 Int64 -> Int64
fence heights = maximum $ fmap (\i -> (lookBoth i) * heights ! i) [lo..hi]
  where (lo, hi)   = bounds heights
        lookDir f w i acc = let v = heights ! i
                                ni = f i
                            in if w <= v && lo <= ni && ni <= hi
                               then lookDir f w (f i) (acc + 1)
                               else acc :: Int64
        lookBack   = lookDir (\x -> x - 1)
        lookFront  = lookDir (+1)
        lookBoth i = let w = heights ! i
                     in lookBack w i 0 + lookFront w i 0 - 1

main :: IO ()
main = getLine >>= \l ->
  putStrLn =<< show . fence . listArray (1, read l) . fmap read . words <$> getLine
