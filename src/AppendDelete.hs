{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Data.Text.IO as Tio

fromString :: String -> T.Text
fromString = T.pack

minDistance :: T.Text -> T.Text -> Int
minDistance t1 t2     = let ln = leading (T.unpack t1) $ T.unpack t2
                        in T.length t1 + T.length t2 - 2 * ln
  where leading [] _  = 0
        leading _ []  = 0
        leading (h1:t1) (h2:t2)
          | h1 == h2  = 1 + leading t1 t2
          | otherwise = 0

appendDelete :: T.Text -> T.Text -> Int -> Bool
appendDelete t1 t2 k
  | k >= l1 + l2 = True
  | k >= mdist   = let n = k - mdist
                 in n `mod` 2 == 0
  | otherwise    = False
  where mdist    = minDistance t1 t2
        l1       = T.length t1
        l2       = T.length t2

main :: IO ()
main = appendDelete <$> Tio.getLine <*> Tio.getLine <*> fmap read getLine >>= \b ->
  if b then putStrLn "Yes" else putStrLn "No"
