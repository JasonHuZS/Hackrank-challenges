import Data.Functor

lcms = 1:[uncurry lcm r | r <- zip nums lcms]
    where nums = [2..]


main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter [] = return ()
          iter (h:t) = (putStrLn $ show $ lcms !! (h - 1)) >> iter t
