{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MultiWayIf #-}
import Data.Functor
import qualified Data.List as List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Array.Unboxed (UArray)
import qualified Data.Array.Unboxed as Arr
import Data.Int (Int64)
import Debug.Trace

-- 
-- derivation
-- how do we count this?
-- let x is the maximum number of the collided number, hence we have
--      x = 2 ^ (k * (k - 1) * r)
--  where k * r = n
-- so we are having (2 ^ (k - 1)) ^ k * r = (2 ^ k) ^ (k - 1) * r
-- if k * r = n, then (k - 1) * r < n satisfies the requirement
-- also, 2 ^ k <= n needs to hold. at strike point, 2 ^ k = n, hence
--      k = lg n
--      r = n / lg n
-- the maximum order would be (lg n - 1) * n ~ n lg n
--
-- hence the thing is to brute force all the numbers that is within
-- n/2*lg n from 2 to n that can be decomposed as x*y, such that
--      b ^ x <= n, y <= n
--  where b is some base number
--


-- |get a list of factors of n that is not 1 and itself
factors :: Integral a => a -> [a]
factors n = uncurry revConcat $ go 2 [1] []
    where !rt = floor $ sqrt $ fromIntegral n
          go !m l1 l2
            | m > rt    = (l1, l2)
            | otherwise = let (d, r) = divMod n m
                           in if | r /= 0    -> go (m+1) l1 l2
                                 | d == m    -> (l1, m:l2)
                                 | otherwise -> go (m+1) (m:l1) (d:l2)


revConcat ::  [a] -> [a] -> [a]
revConcat = flip $ List.foldl' (flip (:))

factorsList n = filter (not . size1 . snd) $ [(i, factors i) | i <- [4..n]]

primes = 2:filter f [3,5..]
    where f n = all (\p -> n `mod` p /= 0) $ takeWhile (\p -> p * p <= n) primes


-- |n >= 2
-- all the numbers that is some order of some number up until n
orderedSet n = go 2 Set.empty
    where go l st
            | l > n             = st
            | Set.member l st   = go (l+1) st
            | Set.null insrtSet = st
            | otherwise         = go (l+1) $ Set.union st insrtSet
            where insrtSet      = Set.fromAscList $ takeWhile (<=n) $ map (l^) [2..]


size1 [_] = True
size1 _   = False

nlogbn b n = n * (floor $ logBase (fromIntegral b) $ fromIntegral n)

baseCandidates n = factorsList limit
    where !limit = nlogbn 2 n

-- |get the # of collided power in cand using base b
collPowBase n b cand = iter b 0 [] cand
    where !logged = floor $ logBase (fromIntegral b) $ fromIntegral n
          !lim    = n * logged
          iter _ !acc l []          = (acc, reverse l)
          iter m !acc l ((o, fs):t) 
            | o > lim               = (acc, reverse l)
            | otherwise             = let list = takeWhile (<=logged) 
                                               $ dropWhile ((>n) . div o) fs
                                          !inc = List.genericLength list
                                       in if inc <= 1 then iter m acc l t
                                                      else iter m (acc + inc - 1) ((o, list):l) t

distPow2 :: Int64 -> Int64
distPow2 n = (n - 1) ^ 2 - iter bases 0 (baseCandidates n)
    where oset                 = orderedSet n
          bases                = filter (not . flip Set.member oset) [2..floor $ sqrt $ fromIntegral n]
          iter []    !acc _    = acc
          iter (b:t) !acc cand = let (m, c) = collPowBase n b cand
                                  in iter t (acc + m) c

main = putStrLn =<< show <$> distPow2 <$> read <$> getLine
