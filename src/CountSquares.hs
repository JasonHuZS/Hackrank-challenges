import Control.Monad

countSquares :: Integral p => p -> p -> p
countSquares a b = let ca = if a' == a then sqrta else sqrta + 1
                       cb = sqrtb
                       n  = cb - ca + 1
                   in n
  where sqrta = floor $ sqrt $ fromIntegral a
        sqrtb = floor $ sqrt $ fromIntegral b
        a'    = sqrta ^ 2
        b'    = sqrtb ^ 2

main :: IO ()
main = getLine >> (lines <$> getContents) >>= \ls ->
  forM_ ls (\l -> let [a, b] = fmap read $ words l
                  in putStrLn $ show $ countSquares a b)
