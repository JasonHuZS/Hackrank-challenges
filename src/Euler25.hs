import Data.Functor

fib = 1:1:zipWith (+) fib (tail fib)

idxfib = zip [1..] $ map (length . show) fib

main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter [] = return ()
          iter (h:t) = (putStrLn $ show $ fst $ head $ filter ((h==) . snd) idxfib) >> iter t
