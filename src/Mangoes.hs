{-# LANGUAGE RecordWildCards #-}
import Debug.Trace
import Data.Maybe


data PTree a = PTree Int a [PTree a]  deriving Show
data PHeap a = PEmpty | PT (PTree a) deriving Show

empty :: PHeap a
empty          = PEmpty
isEmpty :: PHeap a -> Bool
isEmpty PEmpty = True
isEmpty _      = False
insert :: Ord a => a -> PHeap a -> PHeap a
insert e h     = merge (PT $ PTree 1 e []) h

size :: PHeap a -> Int
size PEmpty             = 0
size (PT (PTree s _ _)) = s

merge :: Ord a => PHeap a -> PHeap a -> PHeap a
merge PEmpty h = h
merge h PEmpty = h
merge (PT h1@(PTree s1 e1 hs1)) (PT h2@(PTree s2 e2 hs2))
    | e1 < e2   = PT $ PTree (s1 + s2) e1 (h2:hs1)
    | otherwise = PT $ PTree (s1 + s2) e2 (h1:hs2)

findMin :: PHeap a -> Maybe a
findMin PEmpty                  = Nothing
findMin (PT (PTree _ m _))    = Just m

deleteMin :: Ord a => PHeap a -> Maybe (PHeap a)
deleteMin PEmpty                = Nothing
deleteMin (PT (PTree _ _ hs)) = Just $ mergePairs hs
    where mergePairs []          = PEmpty
          mergePairs [h]         = PT h
          mergePairs (h1:h2:hs') = merge (merge (PT h1) $ PT h2) $ mergePairs hs'


elems :: PHeap a -> [a]
elems PEmpty = []
elems (PT (PTree _ e l)) = e : [ i | t <- l,
                                     i <- elems $ PT t ]

data FSList a = FSList { vol  :: Int
                       , heap :: PHeap a } deriving Show

fslist :: Int -> FSList a
fslist n = FSList n empty

add :: Ord a => FSList a -> a -> FSList a
add l@FSList{..} e
  | size heap + 1 <= vol = l{ heap = insert e heap }
  | otherwise           = let Just m = findMin heap
                          in if e <= m then l
                             else l{ heap = insert e $ fromJust $ deleteMin heap }

to_list :: FSList a -> [a]
to_list = elems . heap


is_viable :: [Int] -> [Int] -> Int -> Int -> Bool
is_viable a h k m = let k'      = k - 1
                        mangoes = map negate $ zipWith ((+) . (k'*)) h a
                        fsl = Prelude.foldr (flip add) (fslist k) mangoes
                    in (size $ heap fsl) == vol fsl && (negate $ sum $ to_list fsl) <= m


guess_k :: [Int] -> [Int] -> Int -> Int -> Int
guess_k a h n m    = do_guess 1 n
  where do_guess lb ub
          | lb == ub  = if is_viable a h lb m then lb else lb - 1
          | otherwise = let k = (lb + ub) `div` 2
                        in if is_viable a h k m
                        then do_guess (k + 1) ub
                        else do_guess lb k

main :: IO ()
main = do
  [n, m] <- readNums
  a      <- readNums
  h      <- readNums
  putStrLn $ show $ guess_k a h n m
  where readNums = fmap read . words <$> getLine
  
