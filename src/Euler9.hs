import Data.Functor

findABC n = iter ccand []
    where ccand          = [n `div` 3 .. (n - 1) `div` 2]
          invar c        = c * c - n * n + 2 * n * c
          iter [] acc    = acc
          iter (c:t) acc = if rt * rt == iv && a2 `mod` 2 == 0 && b2 `mod` 2 == 0
                              then iter t $ [a2 `div` 2, b2 `div` 2, c]:acc
                              else iter t acc
              where iv = invar c
                    rt = floor $ sqrt $ fromIntegral iv
                    a2 = n - c - rt
                    b2 = n - c + rt

main :: IO ()
main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter [] = return ()
          iter (h:t) = (putStrLn $ show $ case findABC h of
                            [] -> -1
                            l  -> maximum $ map (foldl1 (*)) l)
                     >> iter t
