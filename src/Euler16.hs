import Data.Functor

main :: IO ()
main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter [] = return ()
          iter (h:t) = (putStrLn $ show $ sum $ map (\v -> read [v]) $ show $ 2 ^ h) >> iter t
