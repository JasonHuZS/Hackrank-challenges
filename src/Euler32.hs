import Data.Functor
import qualified Data.Char as Char
import Data.Set (Set)
import qualified Data.Set as Set

-- 
-- derivation
-- this implies some constraints by definition,
-- say N = 9, then for two 3-digit numbers that ever have a chance
-- to form a pandigital product will be
-- 135 246, and 135 * 246 = 33210 has 11 digits. hence we can't afford 3x3
-- multiplication even for N = 9.
-- for N = 4, 1x1 is maximum, hence 3 * 4 = 12 is the only one
-- for N = 5, 1x2 is maximum
-- for N = 6, 1x2 is maximum
-- for N = 7, 2x2 is maximum
-- for N = 8, 2x2 is maximum
-- for N = 9, 2x3 is maximum
-- i think within this scale, brute force is feasible.
--


verify n m1 m2 = let r   = m1 * m2
                     s   = show m1 ++ show m2 ++ show r
                     l   = length s
                     set = Set.fromList s
                  in l == Set.size set 
                  && set == (Set.fromList $ map (Char.chr . (48+)) [1..n])

bruteForce n = [(a, b) | a <- [2..99]
                       , b <- [a + 1..9999]
                       , verify n a b]

mulSum l = Set.foldl' (+) 0 $ Set.fromList $ map (uncurry (*)) l

main = putStrLn =<< (show . mulSum . bruteForce . read) <$> getLine
