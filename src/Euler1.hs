import Data.Functor

sumUntil k n
    | n < k = 0
    | otherwise = (k + k * nk) * nk `div` 2
    where nk = (n - 1) `div` k

sumResults n = (sumUntil 3 n) + (sumUntil 5 n) - (sumUntil 15 n)


main :: IO ()
main = do
        getLine
        ls <- lines <$> getContents
        iter ls
    where iter []    = return ()
          iter (h:t) = do
              let n = read h 
              putStrLn $ show $ sumResults n
              iter t
