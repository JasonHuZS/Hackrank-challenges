{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
import qualified Data.List as List
import qualified Data.Map as Map
import Data.Map ((!))
import Data.Functor
import qualified Data.Char as Char
import Debug.Trace

data Op = Nop
        | R
        | L
        | Inc
        | Dec
        | Output
        | Input 
        | Push
        | Pop
        | Loop [Op] deriving Show

data Env = Env{ opc :: Int
              , pir :: Integer
              , sz  :: Integer
              , arr :: Map.Map Integer Int
              } deriving Show


toOp ::  Char -> Op
toOp '>' = R
toOp '<' = L
toOp '+' = Inc
toOp '-' = Dec
toOp '.' = Output
toOp ',' = Input
toOp '[' = Push
toOp ']' = Pop
toOp _   = Nop

isNop :: Op -> Bool
isNop Nop = True
isNop _   = False

emptyEnv ::  Env
emptyEnv = Env{ opc = 100000
              , pir = 0
              , sz  = 1
              , arr = Map.fromList [(0, 0)]
              }

preproc ::  [Op] -> [Op]
preproc []       = []
preproc (Nop:t)  = preproc t
preproc (Push:t) = let (l, r) = f t [] in l:preproc r
    where f (Pop:t) acc  = (Loop $ reverse acc, t)
          f (Push:t) acc = let (l, t') = f t [] in f t' $ l:acc
          f (h:t) acc    = f t $ h:acc
preproc (Pop:t)  = error "impossible"
preproc (h:t)    = h:preproc t


timeout :: String
timeout = "\nPROCESS TIME OUT. KILLED!!!"

exec ::  Env -> [Char] -> [Op] -> IO (Env, [Char], Bool)
-- exec env _ _ | traceShow env False = error ""
exec env inp [] = return (env, inp, True)
exec env inp _ | opc env <= 0 = return (env, inp, False)
exec env inp (Nop:t) = exec env inp t 
exec env inp (L:t)
    | pir env == 0   = error "memory access error!"
    | otherwise      = exec env{ pir = pir env - 1
                               , opc = opc env - 1 } inp t
exec env inp (R:t)   = let s    = sz env
                           p    = pir env + 1
                           env' = if p == s then env{ sz  = s + 1
                                                    , arr = Map.insert s 0 $ arr env }
                                            else env
                        in exec env'{ pir = p
                                    , opc = opc env - 1 } inp t
exec env inp (Inc:t) = let p = pir env
                           e = (arr env) ! p
                        in exec env{ arr = Map.insert p ((e + 1) `mod` 256) $ arr env
                                   , opc = opc env - 1 } inp t
exec env inp (Dec:t) = let p = pir env
                           e = (arr env) ! p
                        in exec env{ arr = Map.insert p ((e + 255) `mod` 256) $ arr env
                                   , opc = opc env - 1 } inp t
exec env inp (Output:t)  = do
                            putChar $ Char.chr $ (arr env) ! (pir env)
                            exec env{ opc = opc env - 1 } inp t
exec env [] (Input:t)    = error "too few input"
exec env (i:l) (Input:t) = exec env{ arr = Map.insert (pir env) (Char.ord i) $ arr env
                                   , opc = opc env - 1 } l t
exec env inp (l@(Loop _):t) = execLoop env inp l t

execLoop ::  Env -> [Char] -> Op -> [Op] -> IO (Env, [Char], Bool)
execLoop env inp _ _
    | opc env <= 0              = return (env, inp, False)
execLoop env inp l@(Loop ops) t = let env1 = env{ opc = opc env - 1 }
                                   in if | (arr env1) ! (pir env1) == 0 -> execLoopEnd env1 inp l t
                                         | otherwise ->               
                                             do
                                                (env2, inp2, _) <- exec env1 inp ops
                                                execLoopEnd env2 inp2 l t

execLoopEnd ::  Env -> [Char] -> Op -> [Op] -> IO (Env, [Char], Bool)
execLoopEnd env inp _ _
    | opc env <= 0      = return (env, inp, False)
execLoopEnd env inp l t = let env2 = env{ opc = opc env - 1 }
                          in if | opc env2 <= 0                -> return (env2, inp, False)
                                | (arr env2) ! (pir env2) /= 0 -> execLoop env2 inp l t
                                | otherwise                    -> exec env2 inp t

--------------------------------------------------------------------------------
--  IO

main ::  IO ()
main = do
    getLine
    inp <- init <$> getLine
    s <- getContents
    let proced = preproc $ filter (not . isNop) $ map toOp s
    -- putStrLn $ show proced
    (e, _, b) <- exec emptyEnv inp $ proced
    if opc e <= 0 && not b then putStrLn timeout
                           else return ()
