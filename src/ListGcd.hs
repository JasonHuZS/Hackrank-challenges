import Data.List as List
import Debug.Trace

fromTwoLists :: (Ord b, Ord a) => [(a, b)] -> [(a, b)] -> [(a, b)]
fromTwoLists [] _ = []
fromTwoLists _ [] = []
fromTwoLists l1@((h1, n1):t1) l2@((h2, n2):t2)
  | h1 < h2  = fromTwoLists t1 l2
  | h1 == h2 = (h1, min n1 n2) : fromTwoLists t1 t2
  | h1 > h2  = fromTwoLists l1 t2


main :: IO ()
main = getLine >> ((uncurry iter) =<< (,) <$> (toNums <$> getLine) <*> (lines <$> getContents))
  where iter l []           = printOut l
        iter l (h:t)        = iter (fromTwoLists l $ toNums h) t
        printOut []         = putStrLn ""
        printOut [(h, n)]   = putNum h >> putStr " " >> putNum n >> putStrLn ""
        printOut ((h, n):t) = putNum h >> putStr " " >> putNum n >> putStr " " >> printOut t
        putNum :: Int -> IO ()
        putNum = putStr . show
        toNums :: String -> [(Int, Int)]
        toNums = tup . fmap read . words
        tup [] = []
        tup (a:b:t)         = (a, b) : tup t
        
