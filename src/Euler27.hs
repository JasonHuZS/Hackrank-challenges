{-# LANGUAGE BangPatterns #-}
import Data.Functor
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

primes :: [Integer]
primes = 2:filter f [3,5..]
    where f n = all (\p -> n `mod` p /= 0) $ takeWhile (\p -> p * p <= n) primes

oddPrimes = tail primes

-- n^2 + a * n + b = p, p in primes(or oddPrimes)
-- hence, when n = 0, b = p, therefore b in primes
-- in general, a = (p - b) / n - n
-- hence the filter is p - b should be divisible starting 2, 3, ...
-- the length of this sequence determined the length of 
-- the resulting consecutive prime list
-- also notice |a| <= N = m, hence to reduce the search range.
--
-- a = (p - b) / n - n <= m
-- p <= n(m + n) + b
--
-- -m <= (p - b) / n - n
-- n(n - m) + b <= p

inRangePrimes m n b = takeWhile (<=ub) $ dropWhile (<lb) oddPrimes
    where lb = n * (n - m) + b
          ub = n * (n + m) + b

setOfA m n b = Set.fromList
             $ fuse
             $ inRangePrimes m n b
    where fuse []    = []
          fuse (p:t) = let (d, m) = divMod (p - b) n
                        in if m == 0 then (d - n):fuse t
                                     else fuse t


intersectionalMerge m1 m2 = Map.fromAscList $ go (Map.toAscList m1) (Map.toAscList m2)
    where go [] _ = []
          go _ [] = []
          go l1@((h1, s1):t1) l2@((h2, s2):t2)
            | h1 < h2              = go t1 l2
            | h1 > h2              = go l1 t2
            | not $ Set.null itsct = (h1, itsct):go t1 t2 -- here h1 == h2
            | otherwise            = go t1 t2
            where itsct = Set.intersection s1 s2


coefsBs m n bs = Map.fromAscList [(b, setOfA m n b) | b <- bs]

coefs m n = coefsBs m n $ takeWhile (<=m) oddPrimes

quadPrimes m = go (coefs m 1) 2
    where go mp n
            | n == m      = error "omg!"
            | Map.null mp = error "something goes definitely wrong!"
            | Map.size mp == 1 && Set.size mset == 1 = (Set.findMin mset, fst $ Map.findMin mp)
            | otherwise = go (intersectionalMerge mp $ coefsBs m n $ Map.keys mp) (n + 1)
            where mset = snd $ Map.findMin mp


main = quadPrimes <$> read <$> getLine >>= (\(a, b) -> putStrLn $ show a ++ " " ++ show b)
