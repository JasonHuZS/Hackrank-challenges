import Data.Functor

sum1st n = (1 + n) * n `div` 2

sum2nd n = n * (n + 1) * (2 * n + 1 ) `div` 6

diff n = abs (s * s - sum2nd n)
    where s = sum1st n

main :: IO ()
main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter []    = return ()
          iter (h:t) = (putStrLn $ show $ diff h) >> iter t
