import Data.Functor

triangNums = 1:zipWith (+) triangNums [2..]

primes = 2:filter f [3,5..]
    where f n = all (\p -> n `mod` p /= 0) $ takeWhile (\p -> p * p <= n) primes

numDivisors 1 = 1
numDivisors n = foldl1 (*) 
              $ map ((+1) . snd) 
              $ iter n 
              $ filter (\p -> n `mod` p == 0) primes
    where factor n p acc
            | n `mod` p /= 0 = (n, acc)
            | otherwise      = factor (n `div` p) p (1 + acc)
          iter 1 _           = []
          iter m (p:ps)
            | p * p > m      = [(m, 1)]
            | otherwise      = let (m', o) = factor m p 0 in (p, o):iter m' ps


divisors = map (\n -> (numDivisors n, n)) triangNums

main :: IO ()
main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter []    = return ()
          iter (h:t) = (putStrLn $ show $ snd $ head $ filter ((>h) . fst) divisors)
                     >> iter t
