{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
import Text.ParserCombinators.Parsec as Par
import Data.List (intercalate)
import qualified Data.Map as Map
import Control.Monad
import Data.Functor ((<$>))
import Data.Foldable (foldlM)


--------------------------------------------------------------------------------
--  Data Structure


data Expr a where
    Var     :: String  -> Expr Integer
    Num     :: Integer -> Expr Integer
    Boolean :: Bool    -> Expr Bool

    -- arithmetic ops
    Add :: Expr Integer -> Expr Integer -> Expr Integer
    Sub :: Expr Integer -> Expr Integer -> Expr Integer
    Mul :: Expr Integer -> Expr Integer -> Expr Integer
    Div :: Expr Integer -> Expr Integer -> Expr Integer

    -- relational ops
    Lt :: Expr Integer -> Expr Integer -> Expr Bool
    Gt :: Expr Integer -> Expr Integer -> Expr Bool

    -- boolean ops
    And :: Expr Bool -> Expr Bool -> Expr Bool
    Or  :: Expr Bool -> Expr Bool -> Expr Bool

    -- this one is for temporary mark for parens
    Grp :: Expr a -> Expr a

instance Show (Expr Integer) where
    show (Var s)     = s
    show (Num n)     = show n
    show (Grp e)     = "(" ++ show e ++ ")"
    show (Add e1 e2) = show e1 ++ " + " ++ show e2

    show (Sub e1 e2@(Add _ _)) = show e1 ++ " - " ++ "(" ++ show e2 ++ ")"
    show (Sub e1 e2@(Sub _ _)) = show e1 ++ " - " ++ "(" ++ show e2 ++ ")"
    show (Sub e1 e2)           = show e1 ++ " - " ++ show e2

    show (Mul (Var s) e)                 = s ++ " * " ++ show e
    show (Mul (Num n) e)                 = show n ++ " * " ++ show e
    show (Mul e1@(Add _ _) e2@(Add _ _)) = "(" ++ show e1 ++ ")" ++ " * " ++ "(" ++ show e2 ++ ")"
    show (Mul e1@(Add _ _) e2)           = "(" ++ show e1 ++ ")" ++ " * " ++ show e2
    show (Mul e1 e2@(Add _ _))           = show e1 ++ " * " ++ "(" ++ show e2 ++ ")"
    show (Mul e1@(Sub _ _) e2@(Sub _ _)) = "(" ++ show e1 ++ ")" ++ " * " ++ "(" ++ show e2 ++ ")"
    show (Mul e1@(Sub _ _) e2)           = "(" ++ show e1 ++ ")" ++ " * " ++ show e2
    show (Mul e1 e2@(Sub _ _))           = show e1 ++ " * " ++ "(" ++ show e2 ++ ")"
    show (Mul e1 e2)                     = show e1 ++ " * " ++ show e2
    show (Div (Var s) e)                 = s ++ " / " ++ show e
    show (Div (Num n) e)                 = show n ++ " / " ++ show e
    show (Div e1@(Sub _ _) e2@(Sub _ _)) = "(" ++ show e1 ++ ")" ++ " / " ++ "(" ++ show e2 ++ ")"
    show (Div e1@(Sub _ _) e2)           = "(" ++ show e1 ++ ")" ++ " / " ++ show e2
    show (Div e1 e2@(Sub _ _))           = show e1 ++ " / " ++ "(" ++ show e2 ++ ")"
    show (Div e1 e2@(Mul _ _))           = show e1 ++ " / " ++ "(" ++ show e2 ++ ")"
    show (Div e1 e2@(Div _ _))           = show e1 ++ " / " ++ "(" ++ show e2 ++ ")"
    show (Div e1 e2)                     = show e1 ++ " / " ++ show e2

instance Show (Expr Bool) where
    show (Boolean b) = if b then "true" else "false"
    show (Lt e1 e2)  = show e1 ++ " < " ++ show e2
    show (Gt e1 e2)  = show e1 ++ " > " ++ show e2
    show (Or e1 e2)  = show e1 ++ " or " ++ show e2
    show (Grp e)     = "(" ++ show e ++ ")"

    show (And e1@(And _ _) e2@(And _ _)) = show e1 ++ " and " ++ show e2
    show (And e1@(And _ _) e2)           = show e1 ++ " and " ++ "(" ++ show e2 ++ ")"
    show (And e1 e2@(And _ _))           = "(" ++ show e1 ++ ")" ++ " and " ++ show e2
    show (And e1 e2)                     = "(" ++ show e1 ++ ")" ++ " and " ++ "(" ++ show e2 ++ ")"


data Stmt = Asnmt  String (Expr Integer)
          | Stmts  [Stmt]
          | Branch (Expr Bool) Stmt Stmt
          | While  (Expr Bool) Stmt

indent ::  Integer -> [Char]
indent i = map (\_ -> ' ') [1..i]

showStmt ::  Stmt -> Integer -> [Char] -> [Char]
showStmt (Asnmt v e) i _       = indent i ++ v ++ " := " ++ show e
showStmt (Branch p e1 e2) i nl = indent i ++ "if ( " ++ show p ++ " ) then" ++ nl
                              ++ indent i ++ "{" ++ nl
                              ++ showStmt e1 (i + 4) nl ++ nl
                              ++ indent i ++ "} else {" ++ nl
                              ++ showStmt e2 (i + 4) nl ++ nl
                              ++ indent i ++ "}"
showStmt (While p e) i nl      = indent i ++ "while ( " ++ show p ++ " ) do" ++ nl
                              ++ indent i ++ "{" ++ nl
                              ++ showStmt e (i + 4) nl ++ nl
                              ++ indent i ++ "}"
showStmt (Stmts sts) i nl      = intercalate (";" ++ nl) $ map (\s -> showStmt s i nl) sts

instance Show Stmt where
    show s = showStmt s 0 "\n"


--------------------------------------------------------------------------------
--  Parser

(<<) :: Parser a -> Parser b -> Parser a
a << b = do { r <- a; b; return r }

keywords ::  [String]
keywords = ["while", "do", "and", "or", "if", "then", "else", "true", "false"]


--------------------------------------------------------------------------------
--  Arithmetic

alpundr :: Parser Char
alpundr = letter <|> char '_' <?> "[a-zA-Z_]"

alpnumdr :: Parser Char
alpnumdr = alpundr <|> digit <?> "[a-zA-Z_0-9]"

var :: Parser String
var = do
    c <- alpundr
    r <- many alpnumdr
    let i = c:r
    if i `elem` keywords
       then unexpected $ i ++ " is a key word!"
       else return i

num :: Parser (Expr Integer)
num = Num <$> read <$> many1 digit

unit :: Parser (Expr Integer)
unit =  (Var <$> var) 
    <|> num 
    <|> (Grp <$> (char '(' >> spaces >> aexp << spaces << char ')'))

factor :: Parser (Expr Integer)
factor = do
    u <- unit << spaces
    o <- optionMaybe $ do
        op <- oneOf "*/" << spaces
        r  <- factor
        return (op, r)
    return $ case o of
        Nothing       -> u
        Just ('*', r) -> Mul u r
        Just ('/', r) -> Div u r

aexp :: Parser (Expr Integer)
aexp = do
    l <- factor << spaces
    o <- optionMaybe $ do
        op <- oneOf "+-" << spaces
        r  <- aexp
        return (op, r)
    return $ case o of
        Nothing       -> l
        Just ('+', r) -> Add l r
        Just ('-', r) -> Sub l r

afinalize ::  Expr Integer -> Expr Integer
afinalize v = let v' = arotate v in f v'
    where f (Grp v)     = v
          f (Add e1 e2) = Add (f e1) $ f e2
          f (Sub e1 e2) = Sub (f e1) $ f e2
          f (Mul e1 e2) = Mul (f e1) $ f e2
          f (Div e1 e2) = Div (f e1) $ f e2
          f v           = v

arotate ::  Expr Integer -> Expr Integer
arotate v@(Var _)            = v
arotate v@(Num _)            = v
arotate (Grp v)              = Grp $ arotate v
arotate (Add e1 (Add e2 e3)) = arotate $ Add (Add (arotate e1) $ arotate e2) $ arotate e3
arotate (Add e1 (Sub e2 e3)) = arotate $ Sub (Add (arotate e1) $ arotate e2) $ arotate e3
arotate (Sub e1 (Add e2 e3)) = arotate $ Add (Sub (arotate e1) $ arotate e2) $ arotate e3
arotate (Sub e1 (Sub e2 e3)) = arotate $ Sub (Sub (arotate e1) $ arotate e2) $ arotate e3
arotate (Mul e1 (Mul e2 e3)) = arotate $ Mul (Mul (arotate e1) $ arotate e2) $ arotate e3
arotate (Mul e1 (Div e2 e3)) = arotate $ Div (Mul (arotate e1) $ arotate e2) $ arotate e3
arotate (Div e1 (Mul e2 e3)) = arotate $ Mul (Div (arotate e1) $ arotate e2) $ arotate e3
arotate (Div e1 (Div e2 e3)) = arotate $ Div (Div (arotate e1) $ arotate e2) $ arotate e3
arotate (Add e1 e2)          = Add (arotate e1) $ arotate e2
arotate (Sub e1 e2)          = Sub (arotate e1) $ arotate e2
arotate (Mul e1 e2)          = Mul (arotate e1) $ arotate e2
arotate (Div e1 e2)          = Div (arotate e1) $ arotate e2

faexp :: Parser (Expr Integer)
faexp = afinalize <$> aexp

--------------------------------------------------------------------------------
--  Boolean

bool :: Parser (Expr Bool)
bool =   (string "true"  >> return (Boolean True)) 
     <|> (string "false" >> return (Boolean False))

rel :: Parser (Expr Bool)
rel = do
    a1 <- faexp
    op <- spaces >> oneOf "<>" << spaces
    a2 <- faexp
    return $ case op of
        '<' -> Lt a1 a2
        '>' -> Gt a1 a2

bunit :: Parser (Expr Bool)
bunit =   try bool 
      <|> try rel 
      <|> (Grp <$> (char '(' >> spaces >> bexp << spaces << char ')'))

bterm :: Parser (Expr Bool)
bterm = do
    l <- bunit << spaces
    o <- optionMaybe $ do
        string "and" >> spaces
        bterm
    return $ case o of
        Nothing -> l
        Just r  -> And l r

bexp :: Parser (Expr Bool)
bexp = do
    l <- bterm << spaces
    o <- optionMaybe $ do
        string "or" >> spaces
        bexp
    return $ case o of
        Nothing -> l
        Just r  -> Or l r

bfinalize ::  Expr Bool -> Expr Bool
bfinalize = f . brotate
    where f (Grp v)     = v
          f (Or e1 e2)  = Or (f e1) $ f e2
          f (And e1 e2) = And (f e1) $ f e2
          f v           = v

-- |even though it could be fine. but it's still better to maintain the
-- normal evaluation order
brotate :: Expr Bool -> Expr Bool
brotate v@(Boolean _)        = v
brotate (Grp v)              = Grp $ brotate v
brotate (Or e1 (Or e2 e3))   = brotate $ Or (Or (brotate e1) $ brotate e2) $ brotate e3
brotate (And e1 (And e2 e3)) = brotate $ And (And (brotate e1) $ brotate e2) $ brotate e3
brotate (Or e1 e2)           = Or (brotate e1) $ brotate e2
brotate (And e1 e2)          = And (brotate e1) $ brotate e2
brotate v                    = v

fbexp :: Parser (Expr Bool)
fbexp = bfinalize <$> bexp


--------------------------------------------------------------------------------
--  Statements Parsing

asn :: Parser Stmt
asn = do
    l <- var
    spaces >> string ":=" << spaces
    r <- faexp
    return $ Asnmt l r

branch :: Parser Stmt
branch = do
    string "if" >> spaces
    p <- fbexp << spaces
    string "then" >> spaces >> char '{' >> spaces
    s1 <- stmt
    spaces >> char '}'
    spaces >> string "else" >> spaces >> char '{' >> spaces
    s2 <- stmt
    spaces >> char '}'
    return $ Branch p s1 s2

while :: Parser Stmt
while = do
    string "while" >> spaces
    p <- fbexp << spaces
    string "do" >> spaces >> char '{' >> spaces
    s <- stmt
    spaces >> char '}'
    return $ While p s

singleStmt :: Parser Stmt
singleStmt =   try branch
           <|> try while
           <|> asn

sepBy' :: Parser a -> Parser b -> Parser [a]
sepBy' p sep = p `sepBy1'` sep <|> return []

sepBy1' :: Parser a -> Parser b -> Parser [a]
sepBy1' p sep = do
    i  <- p
    is <- piter p sep []
    return $ i:reverse is

piter :: Parser a ->  Parser b -> [a] -> Parser [a]
piter p sep acc = do
    m <- (try $ Just <$> (sep >> p)) <|> return Nothing
    case m of
        Nothing -> return acc
        Just i  -> piter p sep $ i:acc

stmt :: Parser Stmt
stmt = do
    ss <- singleStmt `sepBy'` (spaces >> char ';' >> spaces)
    return $ case ss of
        [s] -> s
        _   -> Stmts ss


--------------------------------------------------------------------------------
--  Evaluation

type Env = Map.Map String Integer

emptyEnv :: Env
emptyEnv = Map.empty

aeval :: Env -> Expr Integer -> Either String Integer
aeval _ (Num n)       = return n
aeval env (Add e1 e2) = liftM2 (+) (aeval env e1) $ aeval env e2
aeval env (Sub e1 e2) = liftM2 (-) (aeval env e1) $ aeval env e2
aeval env (Mul e1 e2) = liftM2 (*) (aeval env e1) $ aeval env e2
aeval env (Div e1 e2) = liftM2 div (aeval env e1) $ aeval env e2
aeval env (Var v)     = case Map.lookup v env of
                            Nothing -> Left $ "undefined variable: " ++ v
                            Just n  -> Right n

beval :: Env -> Expr Bool -> Either String Bool
beval _ (Boolean b)   = return b
beval env (Lt e1 e2)  = liftM2 (<) (aeval env e1) $ aeval env e2
beval env (Gt e1 e2)  = liftM2 (>) (aeval env e1) $ aeval env e2
beval env (And e1 e2) = liftM2 (&&) (beval env e1) $ beval env e2
beval env (Or e1 e2)  = liftM2 (||) (beval env e1) $ beval env e2

eval :: Env -> Stmt -> Either String Env
eval env (Asnmt vr val)   = aeval env val >>= (\v -> return $ Map.insert vr v env)
eval env (Branch p s1 s2) = beval env p >>= (\b -> eval env $ if b then s1 else s2)
eval env w@(While p s)    = beval env p >>= (\b -> if b then eval env $ Stmts [s, w] else return env)
eval env (Stmts st)       = foldlM (\e s -> eval e s) env st

--------------------------------------------------------------------------------
--  IO

organize :: Env -> String
organize env = intercalate "\n" $ map (\(k, v) -> k ++ " " ++ show v) $ Map.toAscList env

genOutputs ::  String -> IO ()
genOutputs s = do
    case parse (spaces >> stmt << spaces << eof) "" s of
      Left e  -> putStrLn $ show e
      Right r -> case organize <$> eval emptyEnv r of
                   Left e  -> putStrLn e
                   Right e -> putStrLn e

-- main :: IO ()
-- main = do
--     inputs <- getContents
--     case parse (spaces >> stmt << spaces << eof) "" inputs of
--       Left e  -> putStrLn $ show e
--       Right r -> putStrLn $ show r

main :: IO ()
main = do
    inputs <- getContents
    genOutputs inputs
