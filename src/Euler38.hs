import Data.Functor
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Char as Char
import qualified Data.List as List

isMultiplier k n = go 1 Set.empty
    where kc     = Char.ord $ head $ show k
          go m s
            | Set.size s == k = True
            | otherwise       = let r = m * n
                                    t = show r
                                    l = length t
                                    u = Set.fromList t
                                 in Set.size u == l 
                                 && all ((<=kc) . Char.ord) t 
                                 && (not $ Set.member '0' u)
                                 && Set.null (Set.intersection s u) 
                                 && go (m+1) (Set.union s u)

multipliers k n = filter (isMultiplier k) [2..n - 1]

main = do
    [n, k] <- map read <$> words <$> getLine
    putStrLn $ List.intercalate "\n" $ map show $ multipliers k n
