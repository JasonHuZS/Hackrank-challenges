import Data.Functor

spiral 1 = 1
spiral n = 4 * (s2n - 4 * s2m) - 6 * s22m - 3
    where s2n  = n * (n + 1) * (2 * n + 1) `div` 6
          m    = (n - 1) `div` 2
          s2m  = m * (m + 1) * (2 * m + 1) `div` 6
          s22m = (m + 1) * m


main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter []    = return ()
          iter (h:t) = (putStrLn $ show $ (spiral h) `mod` 1000000007) >> iter t
