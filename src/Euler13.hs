import Data.Functor

main :: IO ()
main = getLine >> (putStrLn =<< take 10 <$> show <$> sum <$> map read <$> lines <$> getContents)
