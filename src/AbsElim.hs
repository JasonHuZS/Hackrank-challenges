import Text.ParserCombinators.Parsec as Par
import Control.Applicative hiding ((<|>), many)
import Control.Monad
import Data.Foldable
import Data.Bifunctor
import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.Set (Set)

--------------------------------------------------------------------------------
--  Data Structures

data Comb = K | S | I | B | C deriving Show

data Expr = Lambda String Expr (Set String)
          | Apply  Expr   Expr (Set String)
          | Ident  String
          | Prim   Comb


instance Show Expr where
    show (Lambda p l@Lambda{} _) = "(\\" ++ p ++ " " ++ (drop 2 $ show l)
    show (Lambda p e _)          = "(\\" ++ p ++ ". " ++ show e ++ ")"
    show (Ident i)               = i
    show (Prim c)                = show c
    show (Apply e1 e2 _)         = "(" ++ show e1 ++ " " ++ show e2 ++ ")"


--------------------------------------------------------------------------------
--  Parsing

alpundr :: Parser Char
alpundr = letter <|> char '_' <?> "[a-zA-Z_]"

alpnumdr :: Parser Char
alpnumdr = alpundr <|> digit <?> "[a-zA-Z_0-9]"

var :: Parser String
var = do
    c <- alpundr
    r <- many alpnumdr
    return $ c:r

plambda :: Parser Expr
plambda = do
    vars <- string "(\\" *> sepBy1 var spaces <* char '.'
    e    <- spaces *> pexpr <* string ")"
    return $ foldr' ($) e $ map (\p e -> Lambda p e Set.empty) vars

papply :: Parser Expr
papply = liftA2 (\e1 e2 -> Apply e1 e2 Set.empty) (char '(' *> pexpr <* spaces) $ pexpr <* char ')'

pident :: Parser Expr
pident = Ident <$> var

pexpr :: Parser Expr
pexpr = pident <|> try plambda <|> papply


--------------------------------------------------------------------------------
--  Processing

freeVars :: Expr -> Set String
freeVars (Ident i)      = Set.singleton i
freeVars (Prim _)       = Set.empty
freeVars (Lambda _ _ v) = v
freeVars (Apply _ _ v)  = v

freeVarProc :: Expr -> Expr
freeVarProc i@Ident{}       = i
freeVarProc p@Prim{}        = p
freeVarProc (Lambda x e _)  = let e' = freeVarProc e
                               in Lambda x e' $ Set.delete x $ freeVars e'
freeVarProc (Apply e1 e2 _) = let e1' = freeVarProc e1
                                  e2' = freeVarProc e2
                               in Apply e1' e2' $ Set.union (freeVars e1') (freeVars e2')

isFree ::  String -> Expr -> Bool
isFree v e = Set.member v $ freeVars e

etaElim :: Expr -> Expr
etaElim (Lambda x (Apply e1 (Ident v) _) _)
    | x == v && not (Set.member x fe1) = e1 -- what if self-application? (\x. x x)
    where fe1 = freeVars e1
etaElim v                              = v

--------------------------------------------------------------------------------
--  Simplification

transform ::  Expr -> Expr
transform e@Prim{}        = e
transform e@(Ident x)     = e
transform (Apply e1 e2 _) = let e1' = transform e1
                                e2' = transform e2
                             in Apply e1' e2' $ Set.union (freeVars e1') (freeVars e2')
transform (Lambda v e _)
    | not $ isFree v e    = let e' = transform e
                             in Apply (Prim K) e' $ freeVars e'
transform (Lambda v1 (Ident v2) _)
    | v1 == v2            = Prim I
transform (Lambda x l@(Lambda y e _) _)
    | isFree x e          = let l' = transform l
                             in transform $ etaElim $ Lambda x l' $ Set.delete x $ freeVars l'

transform (Lambda x (Apply e1 e2 si) so)
    | isFree x e1 && isFree x e2 = Apply (Apply (Prim S) tle1 fvl1) tle2 $ Set.union fvl1 fvl2
    | isFree x e1                = Apply (Apply (Prim C) tle1 fvl1) te2 $ Set.union fvl1 fv2
    | isFree x e2                = case tle2 of
                                    Prim I -> te1 -- CXI = X
                                    _      -> Apply (Apply (Prim B) te1 fv1) tle2 $ Set.union fv1 fvl2
    where tle1 = transform (Lambda x e1 $ freeVars e1)
          fvl1 = freeVars tle1
          tle2 = transform (Lambda x e2 $ freeVars e2)
          fvl2 = freeVars tle2
          te1  = transform e1
          fv1  = freeVars te1
          te2  = transform e2
          fv2  = freeVars te2
          
--------------------------------------------------------------------------------
--  IO

toString ::  Expr -> String
toString (Prim p)              = show p
toString (Apply e1 (Prim p) _) = toString e1 ++ show p
toString (Apply e1 e2 _)       = toString e1 ++ "(" ++ toString e2 ++ ")"

main :: IO ()
main = do
        getLine
        ls <- lines <$> getContents
        iter ls
    where iter [] = return ()
          iter (l:t) = let s = case bimap show 
                                          (toString . transform) 
                                          $ freeVarProc <$> parse pexpr "" l of
                            Left v  -> v
                            Right v -> v
                        in putStrLn s >> iter t
