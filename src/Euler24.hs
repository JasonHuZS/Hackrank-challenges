import Data.Functor
import Data.Set (Set)
import qualified Data.Set as Set

fac 0 = 1
fac n = product [1..n]

nthperm s n = _nthperm set ls n
    where ls = length s
          set = Set.fromList s

_nthperm _ 0 _ = []
_nthperm s l n = go n $ Set.toAscList s
    where fl_1 = fac (l - 1)
          go m (h:t)
            | m <= fl_1 = h:_nthperm (Set.delete h s) (l - 1) m
            | otherwise = go (m - fl_1) t

str = ['a'..'m']

main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter [] = return ()
          iter (h:t) = (putStrLn $ nthperm str h) >> iter t
