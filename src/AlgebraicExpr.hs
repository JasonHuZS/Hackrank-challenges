{-# LANGUAGE LambdaCase #-}

import Text.ParserCombinators.Parsec as P
import Data.Functor
import Data.Int
import qualified Data.Map.Strict as M
import Control.Monad
import Debug.Trace


data NumExpr = Num Int64
             | NAdd NumExpr NumExpr
             | NSub NumExpr NumExpr
             | NMul NumExpr NumExpr
             | NDiv NumExpr NumExpr
             | NExp NumExpr NumExpr deriving Show

data Expr = Var Int64
          | Const NumExpr
          | Add Expr Expr
          | Sub Expr Expr
          | Mul Expr Expr
          | Div Expr NumExpr deriving Show

type NumParser  = Parser NumExpr
type ExprParser = Parser Expr


num :: NumParser
num = fmap (Num . read) (many1 digit)

numterm :: NumParser
numterm = num <|> char '(' *> spaces *> numexpr <* spaces <* char ')'

numexp :: NumParser
numexp = numterm <* spaces >>= \l ->
  many (char '^' *> spaces *> numterm <* spaces) >>= \ns ->
  return $ foldl NExp l ns


numfact :: NumParser
numfact = numexp <* spaces >>= \l ->
  many ((,) <$> oneOf "*/" <* spaces <*> numexp <* spaces) >>= \ts ->
  return $ foldl (\t -> \case ('*', r) -> NMul t r
                              ('/', r) -> NDiv t r) l ts

numexpr :: NumParser
numexpr = numfact <* spaces >>= \l ->
  many ((,) <$> oneOf "+-" <* spaces <*> numfact <* spaces) >>= \fs ->
  return $ foldl (\t -> \case ('+', r) -> NAdd t r
                              ('-', r) -> NSub t r) l fs

var :: ExprParser
var = P.char 'x' <* spaces >> optionMaybe (char '^' *> spaces *> num) >>= \case
  Nothing      -> return $ Var 1
  Just (Num n) -> return $ Var n

term :: ExprParser
term = char '(' *> spaces *> expr <* spaces <* char ')'
       <|> fmap Const numterm
       <|> var

factor :: ExprParser
factor = term <* spaces >>= go
  where go t = (((Mul t) <$> (char '*' *> spaces *> term <* spaces)
               <|> (Div t) <$> (char '/' *> spaces *> numexp <* spaces)
               <|> (Mul t) <$> (spaces *> term <* spaces))
                 >>= go) <|> return t

expr :: ExprParser
expr = factor <* spaces >>= \f ->
  many ((,) <$> oneOf "+-" <* spaces <*> factor <* spaces) >>= \fs ->
  return $ foldl (\t -> \case ('+', r) -> Add t r
                              ('-', r) -> Sub t r) f fs


needParenNum :: NumExpr -> String
needParenNum e@(NAdd _ _) = "(" ++ formatNum e ++ ")"
needParenNum e@(NSub _ _) = "(" ++ formatNum e ++ ")"
needParenNum e            = formatNum e

formatNum :: NumExpr -> String
formatNum (Num i)    = show i
formatNum (NAdd a b) = formatNum a ++ " + " ++ formatNum b
formatNum (NSub a b) = formatNum a ++ " - " ++ formatNum b
formatNum (NMul a b) = needParenNum a ++ " * " ++ needParenNum b
formatNum (NDiv a b) = needParenNum a ++ " / " ++ needParenNum b
formatNum (NExp a b) = sa ++ "^" ++ sb
  where sa = case a of
          Num i -> formatNum a
          NExp _ _ -> formatNum a
          _ -> "(" ++ formatNum a ++ ")"
        sb = case b of
          Num i -> formatNum b
          _ -> "(" ++ formatNum b ++ ")"


needParenExpr :: Expr -> String
needParenExpr e@(Add _ _) = "(" ++ formatExpr e ++ ")"
needParenExpr e@(Sub _ _) = "(" ++ formatExpr e ++ ")"
needParenExpr (Const e)   = needParenNum e
needParenExpr e           = formatExpr e

formatExpr :: Expr -> String
formatExpr (Var i)   = if i == 1 then "x" else "x^" ++ show i
formatExpr (Const c) = formatNum c
formatExpr (Add a b) = formatExpr a ++ " + " ++ formatExpr b
formatExpr (Sub a b) = formatExpr a ++ " - " ++ formatExpr b
formatExpr (Mul (Const (Num n)) v@(Var _)) =
  case n of
    1  -> formatExpr v
    -1 -> '-' : formatExpr v
    _  -> show n ++ formatExpr v
formatExpr (Mul a b) = needParenExpr a ++ " * " ++ needParenExpr b
formatExpr (Div a b) = needParenExpr a ++ " / " ++ needParenNum b


parseExpr :: String -> Either ParseError Expr
parseExpr = parse (spaces *> expr <* eof) ""

r :: Either a b -> b
r (Right e) = e

p :: String -> IO ()
p = putStrLn . formatExpr . r . parseExpr


evalNum :: NumExpr -> Int64
evalNum (Num i)    = i
evalNum (NAdd a b) = evalNum a + evalNum b
evalNum (NSub a b) = evalNum a - evalNum b
evalNum (NMul a b) = evalNum a * evalNum b
evalNum (NDiv a b) = evalNum a `div` evalNum b
evalNum (NExp a b) = evalNum a ^ evalNum b


simplify :: Expr -> M.Map Int64 Int64
simplify (Var i)   = M.singleton i 1
simplify (Const c) = M.singleton 0 $ evalNum c
simplify (Add a b) = mergeAdd (simplify a) (simplify b)
  where mergeAdd   = M.mergeWithKey (\k a b -> Just (a + b)) id id
simplify (Sub a b) = mergeSub (simplify a) (simplify b)
  where mergeSub   = M.mergeWithKey (\k a b-> Just (a - b)) id (fmap negate)
simplify (Mul a b) = M.fromListWith (+) $ [(pa + pb, ca * cb) | (pa, ca) <- al
                                                              , (pb, cb) <- bl]
  where al         = M.toAscList $ simplify a
        bl         = M.toAscList $ simplify b
simplify (Div a b) = let bv = evalNum b
                     in fmap (`div` bv) $ simplify a


mapToExpr :: M.Map Int64 Int64 -> Expr
mapToExpr m
  | M.null m  = Const $ Num 0
  | otherwise = foldl1 (\k -> \case
                           e@(Const (Num n)) | n < 0  -> Sub k $ Const $ Num $ -n
                                             | n >= 0 -> Add k e
                           e@(Mul (Const (Num n)) v)
                             | n < 0  -> Sub k $ (Mul (Const $ Num $ -n) v)
                             | n >= 0 -> Add k $ e)
                $ fmap (\(p, c) -> if p == 0
                                   then (Const $ Num c)
                                   else Mul (Const $ Num c) $ Var p)
                $ M.toDescList m


main :: IO ()
main = getLine >> (lines <$> getContents) >>= \ls ->
  forM_ ls (putStrLn . formatExpr . mapToExpr . simplify . r . parseExpr)
