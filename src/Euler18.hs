import Data.Functor

shrinkTree up down = map sub $ zip up $ zip down $ tail down
    where sub (t, (d1, d2)) = max (t + d1) (t + d2)

findPathR [[v]]   = v
findPathR (d:u:t) = findPathR $ shrinkTree u d:t

main :: IO ()
main = read <$> getLine >>= (\n -> (iter n) =<< lines <$> getContents)
    where iter 0 _      = return ()
          iter n (s:ls) = let m      = read s
                              (a, b) = splitAt m ls
                              tree   = reverse $ map (map read . words) $ a
                           in (putStrLn $ show $ findPathR tree) >> iter (n - 1) b
