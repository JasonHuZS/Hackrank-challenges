import Data.Functor
import Data.Bifunctor
import qualified Data.List as List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

alphaMap = Map.fromList $ zip ['A'..'Z'] [1..]

scores names = Map.fromList $ zip snames $ zipWith (*) [1..] $ map ns snames
    where ns n   = sum $ map (alphaMap Map.!) n
          snames = List.sort names

main :: IO ()
main = do
    n   <- read <$> getLine
    lns <- lines <$> getContents
    let (ns, qs)   = second tail $ splitAt n lns
    let scrMap     = scores ns
    let iter []    = return ()
        iter (h:t) = (putStrLn $ show $ scrMap Map.! h) >> iter t
    iter qs
