import Data.Functor
import Data.Int (Int64)
import Data.Set (Set)
import qualified Data.Set as Set
import Debug.Trace

primes :: [Int64]
primes = 2:filter f [3,5..]
    where f n = all (\p -> n `mod` p /= 0) $ takeWhile (\p -> p * p <= n) primes

limit = 10 ^ 6

primeSet = Set.fromList $ takeWhile (<=limit) primes

isPrime n =  Set.member n primeSet 
          || (n > limit && all (\p -> n `mod` p /= 0) (takeWhile (\p -> p * p <= n) primes))

truncLeft n = go $ show n
    where go []      = True
          go l@(_:t) = (isPrime $ read l) && go t

truncRight n = isPrime n && (go [] $ show n)
    where go _ []    = True
          go l (h:t) = (isPrime $ read $ reverse (h:l)) && go (h:l) t

findTillN n = filter (\v -> truncLeft v && truncRight v) [11..n-1]

main = putStrLn =<< (show . sum . findTillN) <$> read <$> getLine 
