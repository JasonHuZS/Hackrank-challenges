import sys
import functools

sys.setrecursionlimit(1000000)


def cached(func):
    cache = {}

    @functools.wraps(func)
    def wrap(self, n):
        r = func(self, n)
        cache[n] = r
        return r

    return wrap


def from_a(c):
    return ord(c) - 97


def from_str(s):
    return map(from_a, s)


class PrefixTree(object):
    def __init__(self, subtrees=None):
        self.subtrees = subtrees
        self._size = None

    def __getitem__(self, key):
        return self.subtrees[key][1]

    def insert_tree(self, c, i):
        if self.subtrees is None:
            self.subtrees = [None for _ in range(26)]
        if self.subtrees[c] is None:
            self.subtrees[c] = (i, PrefixTree())
        return self.subtrees[c][1]

    def freeze(self):
        if self.subtrees is not None:
            self._size = sum(1 + t[1].freeze()
                             for t in self.subtrees
                             if t is not None)
            return self._size
        else:
            self._size = 0
            return 0

    @property
    def size(self):
        return self._size

    @cached
    def leading_n(self, n):
        if self.subtrees is not None:
            return sum(1 + t[1].leading_n(n)
                       for t in self.subtrees
                       if t is not None and t[0] < n)
        else:
            return 0

    def __repr__(self):
        return "PrefixTree(subtrees={})".format(repr(self.subtrees))


def construct(s, a, b):
    root = PrefixTree()
    trees = []

    i = a
    while i < b:
        c = s[i]
        trees = [t.insert_tree(c, i) for t in trees]
        trees.append(root.insert_tree(c, i))
        i += 1

    root.freeze()
    return root


_, q = map(int, sys.stdin.readline().split())
s = from_str(sys.stdin.readline().strip())
queries = []
for _ in range(q):
    a, b = map(int, sys.stdin.readline().split())
    queries.append((a, b + 1))


qmap = {}
for a, b in queries:
    if a not in qmap:
        qmap[a] = b
    else:
        qmap[a] = max(qmap[a], b)


tree_map = {}
for a, b in qmap.items():
    tree_map[a] = b, construct(s, a, b)


for a, b in queries:
    b2, t = tree_map[a]
    if b2 == b:
        print t.size
    else:
        print t.leading_n(b)
