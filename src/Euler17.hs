import Data.Functor

toWord 0  = "Zero"
toWord 1  = "One"
toWord 2  = "Two"
toWord 3  = "Three"
toWord 4  = "Four"
toWord 5  = "Five"
toWord 6  = "Six"
toWord 7  = "Seven"
toWord 8  = "Eight"
toWord 9  = "Nine"
toWord 10 = "Ten"
toWord 11 = "Eleven"
toWord 12 = "Twelve"
toWord 13 = "Thirteen"
toWord 14 = "Fourteen"
toWord 15 = "Fifteen"
toWord 16 = "Sixteen"
toWord 17 = "Seventeen"
toWord 18 = "Eighteen"
toWord 19 = "Nineteen"
toWord 20 = "Twenty"
toWord 30 = "Thirty"
toWord 40 = "Forty"
toWord 50 = "Fifty"
toWord 60 = "Sixty"
toWord 70 = "Seventy"
toWord 80 = "Eighty"
toWord 90 = "Ninety"
toWord n
    | n >= 1000000000 = toWord (n `div` 1000000000) ++ " Billion" ++ (if n `mod` 1000000000 == 0 then "" else " " ++ toWord (n `mod` 1000000000))
    | n >= 1000000    = toWord (n `div` 1000000) ++ " Million" ++ (if n `mod` 1000000 == 0 then "" else " " ++ toWord (n `mod` 1000000))
    | n >= 1000       = toWord (n `div` 1000) ++ " Thousand" ++ (if n `mod` 1000 == 0 then "" else " " ++ toWord (n `mod` 1000))
    | n >= 100        = toWord (n `div` 100) ++ " Hundred" ++ (if n `mod` 100 == 0 then "" else " " ++ toWord (n `mod` 100))
    | n >= 10         = toWord (n - n `mod` 10) ++ (if n `mod` 10 == 0 then "" else " " ++ toWord (n `mod` 10))

main :: IO ()
main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter [] = return ()
          iter (h:t) = (putStrLn $ toWord h) >> iter t
