import Data.Functor
import Data.Array (Array)
import qualified Data.Array as Arr
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as Map
import Data.Bifunctor

primes = 2:filter f [3,5..]
    where f n = all (\p -> n `mod` p /= 0) $ takeWhile (\p -> p * p <= n) primes

decomp n = iter n primes
    where factor n p acc
            | n `mod` p /= 0 = (n, acc)
            | otherwise      = factor (n `div` p) p (1 + acc)
          iter 1 _           = []
          iter m (p:ps)
            | p * p > m      = [(m, 1)]
            | m `mod` p == 0 = let (m', o) = factor m p 0 in (p, o):iter m' ps
            | otherwise      = iter m ps

divisorSum n = go decp - n
    where decp = decomp n
          go []         = 1
          go ((p, n):t) = let dvs = go t
                           in (p ^ (n+1) - 1) `div` (p - 1) * dvs

sieving n = go Map.empty primes
    where fill p m = Map.unionWith (Map.union) m 
                   $ Map.fromList 
                   $ (takeWhile ((<=n) . fst) $ map (\v -> (p ^ v, Map.fromList [(p, v)])) [1..])
                   >>= (\t -> takeWhile ((<=n) . fst) $ map (\v -> first (*v) t) [1..])
          go m (p:t)
            | n < p     = m
            | otherwise = go (fill p m) t

divisorSums = map (\v -> (v, divisorSum v)) [1..]

sumsArray = Arr.array (1, 100000) $ takeWhile ((<=100000) . fst) divisorSums

sumsArray2 = Arr.array (1, 100000) $ (1, 0):map (\(v, m) -> (v, (sumup . Map.toList) m - v)) (Map.toAscList $ sieving 100000)
    where sumup []         = 1
          sumup ((p, n):t) = let dvs = sumup t
                              in (p ^ (n+1) - 1) `div` (p - 1) * dvs

amicables n = filter (\v -> v < n 
                         && (let s = sumsArray Arr.! v 
                                 ss = if s <= 100000 then sumsArray Arr.! s else divisorSum s
                              in s /= v && ss == v)) [2..n-1]

main :: IO ()
main = getLine >> (iter =<< map read <$> lines <$> getContents)
    where iter []    = return ()
          iter (h:t) = (putStrLn $ show $ sum $ amicables h) >> iter t
