{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
import Text.ParserCombinators.Parsec as P
import Control.Monad
import Data.Foldable hiding (elem)
import Data.Functor
import qualified Data.Set as Set
import Data.Set (Set)
import qualified Data.Map as Map
import qualified Data.Char as Char
import Data.Map (Map)
import Text.Parsec.Prim (Stream, ParsecT)
import Data.List(intercalate, sort)
import Data.Either(rights)
import Prelude hiding (foldl')


--------------------------------------------------------------------------------
--  data structures

data Expr = Ident String
          | Let   String Expr Expr
          | Fun   [String] Expr
          | Call  Expr [Expr]

instance Show Expr where
    show (Ident s)             = s
    show (Let n e1 e2)         = "let " ++ n ++ " = " ++ show e1 ++ " in " ++ show e2
    show (Fun args e)          = "fun " ++ intercalate " " args ++ " -> " ++ show e
    show (Call (Ident f) args) = f ++ "(" ++ (intercalate ", " . map show) args ++ ")"
    show (Call f args)         = "(" ++ show f ++ ") " ++ "(" ++ (intercalate ", " . map show) args ++ ")"

data Param = Noparam
           | Param [Type]

instance Show Param where
    show Noparam                = "()"
    show (Param [TIdt i])       = i
    show (Param [h@(THrt _ _)]) = show h
    show (Param ts)             = "(" ++ (intercalate ", " . map show) ts ++ ")"

data Type = TFun Param    Type   -- a -> b | (a, b..) -> c
          | TGen [String] Type   -- forall[a b] a
          | THrt Type     [Type] -- higher order type
          | TIdt String          -- list

instance Show Type where
    show (TFun p t)  = show p ++ " -> " ++ show t
    show (TGen ts t) = "forall[" ++ intercalate " " ts ++ "] " ++ show t
    show (THrt t ts) = show t ++ "[" ++ (intercalate ", " . map show) ts ++ "]"
    show (TIdt t)    = t


--------------------------------------------------------------------------------
--  tokens

keywords ::  [String]
keywords = ["let", "in", "fun"]

lettok :: Parser String
lettok = string "let"
intok :: Parser String
intok  = string "in"
funtok :: Parser String
funtok = string "fun"


--------------------------------------------------------------------------------
--  parsers

spaces1 :: Parser ()
spaces1 = skipMany1 space

alpundr :: Parser Char
alpundr = letter <|> char '_' <?> "[a-zA-Z_]"

alpnumdr :: Parser Char
alpnumdr = alpundr <|> digit <?> "[a-zA-Z_0-9]"

ident :: Parser String
ident = do
    candidate <- alpundr >>= (\a -> liftM (a:) $ many alpnumdr)
    if candidate `elem` keywords then unexpected (candidate ++ " is a key word!")
                                 else return candidate

then' :: (Stream s m t) => ParsecT s u m a -> ParsecT s u m sep -> ParsecT s u m a
then' p t = do
    r <- p
    t
    return r

(<<) :: (Stream s m t) => ParsecT s u m a -> ParsecT s u m sep -> ParsecT s u m a
(<<) = then'

argList :: Parser [String]
argList = ident `sepBy'` spaces

paramList :: Parser [Expr]
paramList = expr `sepBy'` (spaces >> char ',' >> spaces)

sepBy' :: Parser a -> Parser b -> Parser [a]
sepBy' p sep = p `sepBy1'` sep <|> return []

sepBy1' :: Parser a -> Parser b -> Parser [a]
sepBy1' p sep = do
    i  <- p
    is <- piter p sep []
    return $ i:is

piter :: Parser a ->  Parser b -> [a] -> Parser [a]
piter p sep acc = do
    m <- (try $ liftM Just $ (sep >> p)) <|> return Nothing
    case m of
        Nothing -> return acc
        Just i  -> piter p sep $ i:acc
            
letexpr :: Parser Expr
letexpr = do
    id   <- lettok >> spaces1 >> ident
    spaces >> char '='
    exp  <- spaces >> expr
    exp2 <- spaces1 >> intok >> spaces1 >> expr
    return $ Let id exp exp2

funexpr :: Parser Expr
funexpr = do
    args <- funtok >> spaces1 >> argList
    exp  <- spaces >> string "->" >> spaces >> expr
    return $ Fun args exp

simpexpr ::  Parser Expr
simpexpr = let exp = (char '(' >> spaces >> expr << spaces << char ')') <|> liftM Ident ident
            in do
                h      <- exp
                params <- (char '(' >> paramList << char ')') `sepBy'` spaces
                case params of
                  [] -> return h
                  _  -> return $ foldl' Call h params

expr :: Parser Expr
expr = try letexpr <|> try funexpr <|> simpexpr


parseExpr ::  String -> Either ParseError Expr
parseExpr = parse expr ""

--------------------------------------------------------------------------------
--  type parsers


tyList1 :: Parser [Type]
tyList1 = ty `sepBy1'` (spaces >> char ',' >> spaces)

simpty :: Parser Type
simpty = let h = (char '(' >> spaces >> ty << spaces << char ')') <|> liftM TIdt ident
          in do 
              s  <- h
              ts <- (try $ liftM Just $ char '[' >> tyList1 << char ']') <|> return Nothing
              case ts of
                Nothing  -> return s
                Just ts' -> return $ THrt s ts'

efunty :: Parser Type
efunty = do
    t <- string "()" >> spaces >> string "->" >> spaces >> ty
    return $ TFun Noparam t

functy :: Parser Type
functy = do
    tl <- char '(' >> spaces >> tyList1 << spaces << char ')'
    t  <- spaces >> string "->" >> spaces >> ty
    return $ TFun (Param tl) t

fallty :: Parser Type
fallty = do
    ags <- string "forall[" >> spaces >> argList << spaces << char ']'
    t   <- spaces >> ty
    return $ TGen ags t

ty :: Parser Type
ty = try efunty <|> try functy <|> fallty <|> simt'

simt' :: Parser Type
simt' = do
    h <- simpty
    t <- (try $ liftM Just $ spaces >> string "->" >> spaces >> ty) <|> return Nothing
    case t of
        Nothing -> return h
        Just tf -> return $ TFun (Param [h]) tf

parsety ::  String -> Either ParseError Type
parsety = parse ty ""

--------------------------------------------------------------------------------
--  default env

envStrings ::  [String]
envStrings = [
        "head: forall[a] list[a] -> a",
        "tail: forall[a] list[a] -> list[a]",
        "nil: forall[a] list[a]",
        "cons: forall[a] (a, list[a]) -> list[a]",
        "cons_curry: forall[a] a -> list[a] -> list[a]",
        "map: forall[a b] (a -> b, list[a]) -> list[b]",
        "map_curry: forall[a b] (a -> b) -> list[a] -> list[b]",
        "one: int",
        "zero: int",
        "succ: int -> int",
        "plus: (int, int) -> int",
        "eq: forall[a] (a, a) -> bool",
        "eq_curry: forall[a] a -> a -> bool",
        "not: bool -> bool",
        "true: bool",
        "false: bool",
        "pair: forall[a b] (a, b) -> pair[a, b]",
        "pair_curry: forall[a b] a -> b -> pair[a, b]",
        "first: forall[a b] pair[a, b] -> a",
        "second: forall[a b] pair[a, b] -> b",
        "id: forall[a] a -> a",
        "const: forall[a b] a -> b -> a",
        "apply: forall[a b] (a -> b, a) -> b",
        "apply_curry: forall[a b] (a -> b) -> a -> b",
        "choose: forall[a] (a, a) -> a",
        "choose_curry: forall[a] a -> a -> a"
    ]

decl :: Parser (String, Type)
decl = do
    i <- ident << char ':'
    t <- spaces >> ty << eof
    return (i, t)

parseDecl ::  String -> Either ParseError (String, Type)
parseDecl = parse decl ""

defaultEnv ::  Map String Type
defaultEnv = Map.fromList $ rights $ map parseDecl envStrings


--------------------------------------------------------------------------------
--  type checking


data TypeEnv = TypeEnv { cacc   :: [Char]
                       , tpenv  :: Map String TypeRef
                       , frames :: [TypeStackFrame]
                       } deriving Show

data TypeRef = Unknown
             | Is Type deriving Show

data TypeStackFrame = TSF { edge  :: String -- the smallest temp type of current frame
                          , curge :: Set String -- the set of types that can still be generalized
                          , refby :: Map String String
                          } deriving Show

-- hence TypeEnv guarantees to have at least one frame in the stack
emptyTenv ::  TypeEnv
emptyTenv = TypeEnv ['a'] Map.empty [TSF "_a" Set.empty Map.empty]

curFrame ::  TypeEnv -> TypeStackFrame
curFrame TypeEnv{frames} = head frames

updFrame ::  TypeStackFrame -> TypeEnv -> TypeEnv
updFrame frame tenv@TypeEnv{frames} = tenv{frames = map (\f -> if edge f == edge frame then frame else f) frames}

newFrame ::  TypeEnv -> TypeEnv
newFrame tenv@TypeEnv{..} = tenv{frames = (TSF ('_':reverse cacc) Set.empty Map.empty):frames}

popFrame ::  TypeEnv -> TypeEnv
popFrame tenv@TypeEnv{..} = let te = tenv{frames = tail frames}
                                cf = head frames
                                nf = curFrame te
                                (c, m) = loopOver te (Map.toList $ refby cf) (Set.union (curge nf) $ curge cf) []
                             in updFrame nf{curge = c, refby = m} te
    where loopOver _ [] c m = (c, Map.fromList m)
          loopOver e ((t1, t2):t) c m
            | t2 `lwr` e    = loopOver e t c ((t1, t2):m)
            | otherwise     = loopOver e t (Set.insert t1 c) m

-- |here we are having a trick. if it doesn't belong to the lower frame,
-- then it belongs to current frame. however, the type is not necessarily
-- created in current frame
findFrame ::  String -> TypeEnv -> TypeStackFrame
findFrame t tenv
    | lwr t tenv = findFrame t $ popFrame tenv
    | otherwise  = curFrame tenv

newGen ::  TypeEnv -> (TypeEnv, [Char])
newGen tenv@TypeEnv{..} = (updFrame f{curge = curge'} tenv{cacc = cacc', tpenv = tpenv'}, tname)
    where tname     = '_':reverse cacc
          cacc'     = case head cacc of
                       'z' -> 'a':cacc
                       h   -> (Char.chr $ 1 + Char.ord h):tail cacc
          tpenv'    = Map.insert tname Unknown tpenv
          f@TSF{..} = curFrame tenv
          curge'    = Set.insert tname curge


lwr ::  String -> TypeEnv -> Bool
lwr a tenv = a < (edge $ curFrame tenv)


isTempType :: String -> Bool
isTempType ('_':_) = True
isTempType _       = False


extendType ::  TypeEnv -> Type -> (TypeEnv, Type)
extendType tenv (TGen gs t) = (tenv', rollTypes t)
    where (tenv', ls) = foldl' (\(te, l) g -> let (te', t) = newGen te in (te', (g, t):l)) (tenv, []) gs
          m           = Map.fromList ls
          rollTypes (TFun Noparam t)     = TFun Noparam $ rollTypes t
          rollTypes (TFun (Param tys) t) = TFun (Param $ map rollTypes tys) $ rollTypes t
          rollTypes (THrt t ts)          = THrt (rollTypes t) $ map rollTypes ts
          rollTypes (TIdt s)             = case Map.lookup s m of
                                    Nothing -> TIdt s
                                    Just s' -> TIdt s'
extendType tenv t = (tenv, t)


typeInfer :: Map String Type -> TypeEnv -> Expr -> Either [Char] (TypeEnv, Type)
typeInfer env tenv (Ident n) = case Map.lookup n env of
                                 Nothing -> Left $ "given name " ++ show n ++ " doesn't exist"
                                 Just t  -> return $ extendType tenv t
typeInfer env tenv (Let v e1 e2) = do
    let ctenv = newFrame tenv
    (ctenv', t1) <- typeInfer env ctenv e1
    let t1'   = generalize ctenv' t1
    let tenv' = popFrame ctenv'
    let env'  = if v == "_" then env else Map.insert v t1' env -- ignore '_' by convention
    typeInfer env' tenv' e2 
typeInfer env tenv (Fun [] e) = do
    (te', e') <- typeInfer env tenv e
    return (te', TFun Noparam e')
typeInfer env tenv (Fun ps e) = do
    let (tenv', ls) = foldl' (\(te, l) p -> let (te', t) = newGen te in (te', (p, TIdt t):l)) (tenv, []) ps
    let env'        = Map.union (Map.fromList ls) env
    (te', e') <- typeInfer env' tenv' e
    return (te', TFun (Param $ reverse $ map snd ls) e') 
typeInfer env tenv (Call f ps) = do
    (tenv1, tps)   <- foldrM (\p (te, l) -> (\(te', p') -> (te', p':l)) <$> typeInfer env te p) (tenv, []) ps
    let (tenv2, t) = newGen tenv1
    let inferred   = case tps of
                        [] -> TFun Noparam $ TIdt t
                        _  -> TFun (Param tps) $ TIdt t
    (tenv3, tf)    <- typeInfer env tenv2 f
    tenv4          <- (\m -> m ++ ", for function " ++ show f) <$< typeMatch tenv3 tf inferred
    return (tenv4, TIdt t)


refUpdate ::  String -> String -> TypeStackFrame -> TypeStackFrame
refUpdate t1 t2 f@TSF{..} = let refby' = case Map.lookup t2 refby of
                                                Just t' | t' < t1 -> refby
                                                _ -> Map.insert t2 t1 refby
                             in f{ curge = Set.delete t2 curge
                                 , refby = refby' }


-- |match two unknown types
matchTIdt ::  TypeEnv -> String -> String -> TypeEnv
matchTIdt e@TypeEnv{..} t1 t2
    | t1 < t2   = let frame1 = findFrame t1 e
                      frame2 = findFrame t2 e -- at least higher than frame1
                      tpenv' = Map.insert t1 (Is $ TIdt t2) $ tpenv
                      f2'    = if edge frame1 == edge frame2 -- in this case, free vars remain untouched
                                  then frame2
                                  else refUpdate t1 t2 frame2
                   in updFrame f2' $ e{tpenv = tpenv'}
    | t1 == t2  = e
    | otherwise = matchTIdt e t2 t1

-- |t1 has to be Unknown
typeRef ::  TypeEnv -> String -> Type -> TypeEnv
typeRef e@TypeEnv{..} t1 (TIdt t2)
    | isTempType t2 = case tpenv Map.! t2 of
                        Unknown -> if t1 < t2
                                      then let frame1 = findFrame t1 e
                                               frame2 = findFrame t2 e
                                               f2'    = if edge frame1 == edge frame2
                                                           then frame2
                                                           else refUpdate t1 t2 frame2
                                            in updFrame f2' e
                                      else e
                        Is t2'  -> typeRef e t1 t2'
    | otherwise     = e
typeRef e t1 (THrt ti2 ts2)       = foldl' (\e' t' -> typeRef e' t1 t') e (ti2:ts2)
typeRef e t1 (TFun Noparam rt)    = typeRef e t1 rt
typeRef e t1 (TFun (Param ps) rt) = foldl' (\e' t' -> typeRef e' t1 t') e (rt:ps)


travRefby ::  TypeEnv -> String -> String
travRefby e t = let f = findFrame t e
                 in case Map.lookup t $ refby f of
                      Nothing -> t
                      Just t' -> travRefby e t'

-- fun x -> let f1 = fun y -> let f2 = fun z -> eq(y, x) in f2 in f1
-- fun x -> let f = fun g y -> let _ = g(y) in eq(x, g) in f

typeMatch :: TypeEnv -> Type -> Type -> Either String TypeEnv
typeMatch e@TypeEnv{..} (TIdt t1) (TIdt t2)
    | isTempType t1 && isTempType t2 = case (tpenv Map.! t1, tpenv Map.! t2) of 
                                         (Unknown, Unknown)  -> return $ matchTIdt e t1 t2
                                         (Is t1', Is t2')    -> typeMatch e t1' t2'
                                         ((Is t1'), Unknown) -> typeMatch e t1' $ TIdt t2
                                         (Unknown, (Is t2')) -> typeMatch e (TIdt t1) t2'
    | isTempType t1                  = case tpenv Map.! t1 of
                                         Unknown  -> return e{tpenv = Map.insert t1 (Is $ TIdt t2) tpenv} -- it means t2 is not temp type
                                         (Is t1') -> typeMatch e t1' $ TIdt t2
    | isTempType t2                  = case tpenv Map.! t2 of
                                         Unknown  -> return e{tpenv = Map.insert t2 (Is $ TIdt t1) tpenv} -- it means t1 is not temp type
                                         (Is t2') -> typeMatch e (TIdt t1) t2'
    | t1 == t2                       = return e
    | otherwise                      = Left $ "these two types cannot match: " ++ t1 ++ " and " ++ t2
typeMatch e@TypeEnv{..} (TIdt t1) t2
    | isTempType t1                  = case tpenv Map.! t1 of
                                         Unknown  -> return $ typeRef e{tpenv = Map.insert t1 (Is t2) tpenv} (travRefby e t1) t2
                                         (Is t1') -> typeMatch e t1' t2
    | otherwise                      = Left $ "these two types cannot match: " ++ show t1 ++ " and " ++ show t2
typeMatch e t1 t2@(TIdt _)           = typeMatch e t2 t1
typeMatch e t1@(THrt ti1 ts1) t2@(THrt ti2 ts2) =
    if length ts1 /= length ts2
       then Left $ "these two types can't match because they are not the same generic type: " ++ show t1 ++ " and " ++ show t2
       else do
           e1 <- typeMatch e ti1 ti2
           e2 <- (\m -> m ++ ", when comparing types " ++ show ts1 ++ " and " ++ show ts2) 
                <$< (foldlM (\ee (tt1, tt2) -> typeMatch ee tt1 tt2) e1 $ zip ts1 ts2)
           return e2
typeMatch e (TFun Noparam rt1) (TFun Noparam rt2) = typeMatch e rt1 rt2
typeMatch _ t1@(TFun Noparam _) t2@(TFun _ _)     = Left $ "type " ++ show t1 ++ " doesn't receive param, cannot match type " ++ show t2
typeMatch _ t1@(TFun _ _) t2@(TFun Noparam _)     = Left $ "type " ++ show t2 ++ " doesn't receive param, cannot match type " ++ show t1
typeMatch e t1@(TFun (Param pt1) rt1) t2@(TFun (Param pt2) rt2) = 
    if length pt1 /= length pt2
       then Left $ "these two types can't be the same as their param lists don't share the same length: " ++ show t1 ++ " and " ++ show t2
       else do
           e1 <- (\m -> m ++ ", as return types") <$< typeMatch e rt1 rt2
           e2 <-(\m -> m ++ ", when matching param types: " ++ show pt1 ++ " and " ++ show pt2) 
                <$< (foldlM (\ee (tt1, tt2) -> typeMatch ee tt1 tt2) e1 $ zip pt1 pt2)
           return e2
typeMatch _ t1 t2 = Left $ "cannot match these two types in general: " ++ show t1 ++ " and " ++ show t2



generalize ::  TypeEnv -> Type -> Type
generalize tenv rt = let (t, m, _) = _generalize tenv rt Map.empty (Char.ord 'a') 
                         generics  = sort $ map snd $ Map.toList m
                      in case generics of
                           [] -> t
                           _  -> TGen generics t


_generalize :: TypeEnv -> Type -> Map String String -> Int -> (Type, Map String String, Int)
_generalize e@TypeEnv{frames = TSF{..}:_, ..} t@(TIdt n) fm cn
    | Map.member n fm          = (TIdt $ fm Map.! n, fm, cn)
    | not $ isTempType n       = (t, fm, cn)
    | not $ Set.member n curge = (t, fm, cn)
    | otherwise                = case tpenv Map.! n of
                            Unknown -> (TIdt [Char.chr cn], Map.insert n [Char.chr cn] fm, cn + 1)
                            Is t'   -> _generalize e t' fm cn
_generalize tenv (TFun Noparam t) fm cn =
    let (t', fm', cn') = _generalize tenv t fm cn
     in (TFun Noparam t', fm', cn')
_generalize tenv (TFun (Param ps) t) fm cn = 
    let (rps', m', c')  = foldl' acc ([], fm, cn) ps
        ps'             = reverse rps'
        (t', m'', c'')  = _generalize tenv t m' c'
     in (TFun (Param ps') t', m'', c'')
    where acc (pss, fmm, cnn) p = let (p', fm', cn') = _generalize tenv p fmm cnn 
                                   in (p':pss, fm', cn')
_generalize tenv (THrt t ts) m c =
    let acc (l, m, c) p  = let (p', m', c') = _generalize tenv p m c in (p':l, m', c')
        (t', m', c')     = _generalize tenv t m c
        (rts', m'', c'') = foldl' acc ([], m', c') ts
        ts'              = reverse rts'
    in (THrt t' ts', m'', c'')

--------------------------------------------------------------------------------
--  helpers

(<$<) :: (a -> b) -> Either a c -> Either b c
f <$< (Left r)  = Left $ f r
_ <$< (Right r) = Right r

--------------------------------------------------------------------------------
--  IO
infer ::  String -> Either String Type
infer exp = do
    r <- show <$< parseExpr exp
    (e, t) <- typeInfer defaultEnv emptyTenv r 
    return $ generalize e t

main :: IO ()
main = do
    exp <- getLine
    case infer exp of
      Left msg -> putStrLn msg
      Right r  -> putStrLn $ show r
