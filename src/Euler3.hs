import Data.Functor

primes = 2:filter f [3,5..]
    where f n = all (\p -> n `mod` p /= 0) $ takeWhile (\p -> p * p <= n) primes


lprime n = iter n primes 1
    where iter n ps a = case primeDiv n ps of
                            []    -> max n a
                            (h:_) -> iter (divall n h) (tail ps) h
          primeDiv n' = filter (\p -> n' `mod` p == 0) . takeWhile (\p -> p * p <= n')
          divall a b
            | a `mod` b /= 0 = a
            | otherwise      = divall (a `div` b) b

main :: IO ()
main = do
        getLine
        ls <- map read <$> lines <$> getContents
        iter ls
    where iter []    = return ()
          iter (h:t) = (putStrLn $ show $ lprime h) >> iter t
