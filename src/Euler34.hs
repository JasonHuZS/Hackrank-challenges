{-# LANGUAGE BangPatterns #-}
import Data.Functor
import Data.Int (Int64)
import Data.Array.Unboxed (UArray)
import qualified Data.Array.Unboxed as Arr

factorials :: UArray Int64 Int64
factorials = Arr.array (0, 9) $ (0, 1):[(n, product [1..n]) | n <- [1..9]]

verify n = (iter n 0) `mod` n == 0
    where iter n !acc
            | n < 10 = acc + factorials Arr.! n
            | otherwise = let (d, r) = divMod n 10
                           in iter d (acc + factorials Arr.! r)


main = putStrLn =<< (\n -> show $ sum $ filter verify [10..n]) <$> read <$> getLine
