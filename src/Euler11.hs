import qualified Data.Array as Arr
import Data.Functor

colmax mat = maximum
           $ map (foldl1 (*) . map (mat Arr.!))
           $ [[(x, y - k) | k <- [0..3]] | x <- [0..19], y <- [0..19]]


rowmax mat = maximum
           $ map (foldl1 (*) . map (mat Arr.!))
           $ [[(x - k, y) | k <- [0..3]] | x <- [0..19], y <- [0..19]]


diagmax1 mat = maximum
             $ map (foldl1 (*) . map (mat Arr.!))
             $ [[(x + k, y - k) | k <- [0..3]] | x <- [0..19], y <- [0..19]]

diagmax2 mat = maximum
             $ map (foldl1 (*) . map (mat Arr.!))
             $ [[(x - k, y - k) | k <- [0..3]] | x <- [0..19], y <- [0..19]]


main :: IO ()
main = do
    let mat = Arr.array ((-3, -3), (22, 19)) [((x, y), 0) | x <- [-3..22], y <- [-3..19]]
    upt <-  ((\(r, l) -> map (\(c, v) -> ((r, c), v)) l) =<<)
        <$> zip [0..] 
        <$> map (zip [0..] . map read . words) 
        <$> lines <$> getContents 
    let nmat = mat Arr.// upt
    putStrLn $ show $ maximum $ map ($ nmat) [colmax, rowmax, diagmax1, diagmax2]
