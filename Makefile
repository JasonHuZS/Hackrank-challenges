src:=src
challenge_files:=$(wildcard ${src}/*.hs)
challenges:=$(patsubst ${src}/%.hs,%,${challenge_files})

PROFILE:=false
O2:=false
GHC:=ghc
_GHCARGS:=
RUNARGS:=
GHCARGS:=

ifeq (${PROFILE}, true)
	_GHCARGS:=${_GHCARGS} -prof -fprof-auto -rtsopts
	RUNARGS:=${RUNARGS} +RTS -p -s
endif

ifeq (${O2}, true)
	_GHCARGS:=${_GHCARGS} -O2
endif

.PHONY: all clean

all:
	@echo "list of challenges: "
	@echo ${challenges} | tr ' ' '\n' | sort

${challenges}: %: ${src}/%.hs
	${GHC} ${GHCARGS} ${_GHCARGS} $^

$(addprefix run-,${challenges}): run-%: %
	${src}/$^ ${RUNARGS}

clean:
	@echo "clean up generated files..."
	-rm ${src}/*.hi ${src}/*.o 2>/dev/null || true
	-rm *.prof 2>/dev/null || true
	-find ${src} -type f -executable -exec rm {} +
