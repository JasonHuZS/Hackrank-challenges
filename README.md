# Hackrank-challenges

implementation for various Hackrank challenges

## Haskell

following projects are written in Haskell:

* WhileLang: While Language
* TypeInfer: Type Inference
* ExprV2: Expression V2
* BrainFuck: Brain Fuck Interpreter
* AbsElim: Down With Abstractions

* Project Euler:
    * Project Euler #1 : Multiples of 3 and 5
    * Project Euler #2 : Even Fibonacci numbers
    * Project Euler #3 : Largest prime factor
    * Project Euler #4 : Largest palindrome product
    * Project Euler #5 : Smallest multiple
    * Project Euler #6 : Sum square difference
    * Project Euler #7 : 10001st prime
    * Project Euler #8 : Largest product in a series
    * Project Euler #9 : Special Pythagorean triplet
    * Project Euler #10: Summation of primes
    * Project Euler #11: Largest product in a grid
    * Project Euler #12: Highly divisible triangular number
    * Project Euler #13: Large sum
    * Project Euler #14: Longest Collatz sequence
    * Project Euler #15: Lattice paths
    * Project Euler #16: Power digit sum
    * Project Euler #17: Number to Words
    * Project Euler #18: Maximum path sum I
    * Project Euler #20: Factorial digit sum
    * Project Euler #21: Amicable numbers 
    * Project Euler #22: Names scores
    * Project Euler #23: Non-abundant sums
    * Project Euler #24: Lexicographic permutations
    * Project Euler #25: N-digit Fibonacci number
    * Project Euler #26: Reciprocal cycles
    * Project Euler #27: Quadratic primes
